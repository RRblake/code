library	ieee;
use ieee.std_logic_1164.all;  


-------------------------------------------------

entity controlpath is				
port(	
	pc_cp : in std_logic_vector(31 downto 0);
	beq_sel_cp : out std_logic;
	out_to_reg_cp : out std_logic;
	mem_rewr_cp : out std_logic;
	writeEn_cp : out std_logic;	
	writeRegsel_1_cp : out std_logic_vector(3 downto 0);
	writeRegsel_2_cp : out std_logic_vector(3 downto 0);  
	write_reg_sel_cp : out std_logic; 
	regAsel_cp : out std_logic_vector(3 downto 0);
	regBsel_cp : out std_logic_vector(3 downto 0);
	alu_src_cp : out std_logic;
	alu_S_cp : out std_logic_vector(2 downto 0);
	immd_cp : out std_logic_vector(15 downto 0)
);
end controlpath;

architecture struct of controlpath is

component instru_mem is
port ( 		
		address	: in std_logic_vector(31 downto 0);
		data_out : out std_logic_vector(31 downto 0)
);
end component;

component main_cntrl is
port(
	op : in std_logic_vector(5 downto 0);
	alu_src : out std_logic;
	mem_rewr : out std_logic;
	out_to_reg : out std_logic;
	writeEn : out std_logic;
	beq_sel : out std_logic;
	write_reg_sel : out std_logic;
	alu_op : out std_logic_vector(3 downto 0)
);
end component;

component alu_cntrl is
port(
	op_alu : in std_logic_vector(3 downto 0);
	func : in std_logic_vector(5 downto 0);
	alu_s : out std_logic_vector(2 downto 0)
);
end component;

signal data_bus : std_logic_vector(31 downto 0);
signal alu_sel_bus : std_logic_vector(3 downto 0);

begin
	U1: instru_mem port map(pc_cp, data_bus);
	
	U2: main_cntrl port map(data_bus(29 downto 24), alu_src_cp, mem_rewr_cp, out_to_reg_cp, writeEn_cp, beq_sel_cp, write_reg_sel_cp, alu_sel_bus);

	U3: alu_cntrl port map(alu_sel_bus, data_bus(11 downto 6), alu_S_cp);

	immd_cp <= data_bus(15 downto 0);
	writeRegsel_1_cp <= data_bus(15 downto 12);
	writeRegsel_2_cp <= data_bus(19 downto 16);
	regAsel_cp <= data_bus(23 downto 20);
	regBsel_cp <= data_bus(19 downto 16);
	
end struct;