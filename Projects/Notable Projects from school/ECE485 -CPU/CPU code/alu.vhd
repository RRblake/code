---------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


---------------------------------------------------

entity alu is

port(	A : in std_logic_vector(31 downto 0);
	B : in std_logic_vector(31 downto 0);
	S : in std_logic_vector(2 downto 0);
	F : out std_logic_vector(31 downto 0);
	beq_F : out std_logic 
);

end ALU;

---------------------------------------------------

architecture behv of alu is
begin					   

    process(A,B,S)
    begin
    
	-- use case statement to achieve 
	-- different operations of ALU

	case S is
	    when "000" =>
		F <= A + B;
	    when "001" =>						
	        F <= A + (not B) + 1;
		if ((A + (not B) + 1) = 0)then
			beq_F <= '1';
		else
			beq_F <= '0';
		end if;
            when "010" =>
		F <= A and B;
	    when "011" =>	 
		F <= A or B;
	    when "100" =>	 
		F <= A nand B;
	    when others =>	 
		F <= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        end case;

	

    end process;

end behv;

----------------------------------------------------
