library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------

entity Mux is
port(	
	mux_A: 	in std_logic_vector(31 downto 0);
	mux_B: 	in std_logic_vector(31 downto 0);
	mux_S:	in std_logic;
	mux_F:	out std_logic_vector(31 downto 0)
);
end Mux;  

-------------------------------------------------

architecture behv of Mux is
begin
    process(mux_A, mux_B, mux_S)
    begin
    
        -- use case statement
        case mux_S is
	    when '0' => mux_F <= mux_A;
	    when '1' => mux_F <= mux_B;
    	    when others => mux_F <= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	end case;

    end process;
end behv;