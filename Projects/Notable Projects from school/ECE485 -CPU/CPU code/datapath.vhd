library	ieee;
use ieee.std_logic_1164.all;  
		   







-------------------------------------------------

entity datapath is				
port(	
	beq_sel_dp : in std_logic;
	out_to_reg_dp : in std_logic;
	mem_rewr_dp : in std_logic;
	immd_dp : in std_logic_vector (15 downto 0);
	clock_dp	: 	in std_logic; 	
	writeEn_dp	: 	in std_logic;	
	writeRegsel_1_dp	: 	in std_logic_vector(3 downto 0);
	writeRegsel_2_dp	: 	in std_logic_vector(3 downto 0);  
	write_reg_sel_dp : in std_logic; 
	regAsel_dp	: 	in std_logic_vector(3 downto 0);
	regBsel_dp	: 	in std_logic_vector(3 downto 0);
	alu_src_dp : in std_logic;
	alu_S_dp : in std_logic_vector(2 downto 0);
	alu_out_dp : out std_logic_vector(31 downto 0);
	pc_dp : out std_logic_vector(31 downto 0)
);
end datapath;

-------------------------------------------------

architecture struct of datapath is

component reg_file is
port ( 	
	clock	: 	in std_logic; 	
	writeEn	: 	in std_logic;	
	writeRegsel	: 	in std_logic_vector(3 downto 0);  
	regAsel	: 	in std_logic_vector(3 downto 0);
	regBsel	: 	in std_logic_vector(3 downto 0);
	writeIn	: 	in std_logic_vector(31 downto 0);
	outA	: 	out std_logic_vector(31 downto 0);
	outB	:	out std_logic_vector(31 downto 0)
);
end component;

component alu is
port (	
	A:	in std_logic_vector(31 downto 0);
	B:	in std_logic_vector(31 downto 0);
	S:	in std_logic_vector(2 downto 0);
	F:	out std_logic_vector(31 downto 0);
	beq_F : out std_logic   
);
end component;

component Mux is
port (
	mux_A: 	in std_logic_vector(31 downto 0);
	mux_B: 	in std_logic_vector(31 downto 0);
	mux_S:	in std_logic;
	mux_F:	out std_logic_vector(31 downto 0)
);
end component; 

component Mux_1 is
port(	
	mux_A_1: 	in std_logic;
	mux_B_1: 	in std_logic;
	mux_S_1:	in std_logic;
	mux_F_1:	out std_logic
);
end component;

component Mux_4 is
port(	
	mux_A_4: 	in std_logic_vector(3 downto 0);
	mux_B_4: 	in std_logic_vector(3 downto 0);
	mux_S_4:	in std_logic;
	mux_F_4:	out std_logic_vector(3 downto 0)
);
end component;  

component zero_ext is
port(
	I : in std_logic_vector (15 downto 0);
	O : out std_logic_vector (31 downto 0)
);
end component;

component data_mem is
port(
	clock : in std_logic;
	mem_re_en : in std_logic;
	mem_wr_en : in std_logic;
	address	: in std_logic_vector(31 downto 0);
	data_in	: in std_logic_vector(31 downto 0);
	data_out : out std_logic_vector(31 downto 0)
);
end component;

component adder is
port(
	addA : in std_logic_vector(31 downto 0);
	addB : in std_logic_vector(31 downto 0);
	addF : out std_logic_vector(31 downto 0)
);
end component;

component pc is
port(
	clock	: 	in std_logic; 	  
	inPC	: 	in std_logic_vector(31 downto 0);
	outPC	: 	out std_logic_vector(31 downto 0)

);
end component;



signal ext_mux : std_logic_vector(31 downto 0);
signal rf_muxAndMem_B : std_logic_vector(31 downto 0);
signal mux_alu_B : std_logic_vector(31 downto 0);
signal rf_alu_A : std_logic_vector(31 downto 0); 
signal alu_F : std_logic_vector(31 downto 0);
signal out_sig : std_logic_vector(31 downto 0);
signal mem_out : std_logic_vector(31 downto 0);
signal inv_memrewr : std_logic;
signal new_pc : std_logic_vector(31 downto 0);
signal adder_mux_pc : std_logic_vector(31 downto 0);
signal extshft_mux_pc : std_logic_vector(31 downto 0);
signal mux_pc : std_logic_vector(31 downto 0);
signal equal_beq : std_logic;
signal write_reg_signal : std_logic_vector(3 downto 0);
signal beq_sel_pc : std_logic;
begin		

  inv_memrewr <= not(mem_rewr_dp);

  U1: reg_file port map(clock_dp, writeEn_dp, write_reg_signal, 
			regAsel_dp, regBsel_dp, 
			out_sig, rf_alu_A, rf_muxAndMem_B);

  U2: alu port map(rf_alu_A, mux_alu_B, alu_S_dp, alu_F, equal_beq);
  
  U3: Mux port map(rf_muxAndMem_B, ext_mux, alu_src_dp, mux_alu_B);

  U4: zero_ext port map(immd_dp, ext_mux);

  U5: Mux port map(alu_F, mem_out, out_to_reg_dp, out_sig);

  U6: data_mem port map(clock_dp, inv_memrewr, mem_rewr_dp, alu_F, rf_muxAndMem_B, mem_out);

  U7: adder port map("00000000000000000000000000000001", new_pc, adder_mux_pc);

  U8: pc port map(clock_dp, mux_pc, new_pc);

  U9: zero_ext port map(immd_dp, extshft_mux_pc);

  U10: Mux port map(adder_mux_pc, extshft_mux_pc, beq_sel_pc, mux_pc);

  U11: Mux_4 port map(writeRegsel_1_dp, writeRegsel_2_dp, write_reg_sel_dp, write_reg_signal);

  U12: Mux_1 port map('0', equal_beq, beq_sel_dp, beq_sel_pc);

  pc_dp <= new_pc;
  alu_out_dp <= out_sig;

	
end struct;

