library	ieee;
use ieee.std_logic_1164.all;  

entity cpu is
port(
	clk : in std_logic;
	cpu_out : out std_logic_vector(31 downto 0)
);
end cpu;

architecture struct of cpu is

component datapath is
port(
	beq_sel_dp : in std_logic;
	out_to_reg_dp : in std_logic;
	mem_rewr_dp : in std_logic;
	immd_dp : in std_logic_vector (15 downto 0);
	clock_dp	: 	in std_logic; 	
	writeEn_dp	: 	in std_logic;	
	writeRegsel_1_dp	: 	in std_logic_vector(3 downto 0);
	writeRegsel_2_dp	: 	in std_logic_vector(3 downto 0);  
	write_reg_sel_dp : in std_logic; 
	regAsel_dp	: 	in std_logic_vector(3 downto 0);
	regBsel_dp	: 	in std_logic_vector(3 downto 0);
	alu_src_dp : in std_logic;
	alu_S_dp : in std_logic_vector(2 downto 0);
	alu_out_dp : out std_logic_vector(31 downto 0);
	pc_dp : out std_logic_vector(31 downto 0)
);
end component;

component controlpath is
port(
	pc_cp : in std_logic_vector(31 downto 0);
	beq_sel_cp : out std_logic;
	out_to_reg_cp : out std_logic;
	mem_rewr_cp : out std_logic;
	writeEn_cp : out std_logic;	
	writeRegsel_1_cp : out std_logic_vector(3 downto 0);
	writeRegsel_2_cp : out std_logic_vector(3 downto 0);  
	write_reg_sel_cp : out std_logic; 
	regAsel_cp : out std_logic_vector(3 downto 0);
	regBsel_cp : out std_logic_vector(3 downto 0);
	alu_src_cp : out std_logic;
	alu_S_cp : out std_logic_vector(2 downto 0);
	immd_cp : out std_logic_vector(15 downto 0)
);
end component;

signal pc : std_logic_vector(31 downto 0);
signal beq_sel : std_logic;
signal out_to_reg : std_logic;
signal mem_rewr : std_logic;
signal writeEn : std_logic;
signal writeRegsel_1 : std_logic_vector(3 downto 0);
signal writeRegsel_2 : std_logic_vector(3 downto 0);
signal write_reg_sel : std_logic;
signal regAsel : std_logic_vector(3 downto 0);
signal regBsel : std_logic_vector(3 downto 0);
signal alu_src : std_logic;
signal alu_S : std_logic_vector(2 downto 0);
signal immd : std_logic_vector(15 downto 0);

begin
	U1: datapath port map(beq_sel, out_to_reg, mem_rewr, immd, clk, writeEn, writeRegsel_1,
				writeRegsel_2, write_reg_sel, regAsel, regBsel, alu_src, alu_S,
				cpu_out, pc);

	U2: controlpath port map(pc, beq_sel, out_to_reg, mem_rewr, writeEn, writeRegsel_1,
				writeRegsel_2, write_reg_sel, regAsel, regBsel, alu_src, alu_S,
				immd);
end struct;
