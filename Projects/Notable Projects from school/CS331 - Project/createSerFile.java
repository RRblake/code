import java.io.*;
import java.util.*;
public class createSerFile
{
	public static void main(String [] args)
	{
		Coupon c1 = new Coupon("Groupon", "Milk", 2.50, .3, 100, false);
		Coupon c2 = new Coupon("DealDaddie", "Eggs", 10.50, .1, 50, false);
		Coupon c3 = new Coupon("DealDaddie", "Juice", 6.00, .45, 12, false);
		Coupon c4 = new Coupon("Living Social", "Cereal", 8.75, .05, 200, false);
		Coupon c5 = new Coupon("Groupon", "Steak", 32.60, .18, 60, false);
		Coupon c6 = new Coupon("Groupon", "Cheese", 4.59, .62, 600, false);
		Coupon c7 = new Coupon("DealDaddie", "Pillow", 14.99, .05, 150, false);
		Coupon c8 = new Coupon("DealDaddie", "Video Game", 60.00, .20, 31, false);
		Coupon c9 = new Coupon("Living Social", "Chair", 65.00, .01, 365, false);
		Coupon c10 = new Coupon("Groupon", "Computer", 600.00, .20, 2, false);
		
		try
		{
			FileOutputStream fos = new FileOutputStream("Coupons.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(c1);
			oos.writeObject(c2);
			oos.writeObject(c3);
			oos.writeObject(c4);
			oos.writeObject(c5);
			oos.writeObject(c6);
			oos.writeObject(c7);
			oos.writeObject(c8);
			oos.writeObject(c9);
			oos.writeObject(c10);
			oos.close();
			
		}
		catch (IOException e)
		{
			System.out.print("Error: " + e);
		}
	}
}