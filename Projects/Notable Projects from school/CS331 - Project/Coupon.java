import java.util.*;
import java.io.*;
import java.text.*;
public class Coupon implements Serializable
{
	String provider;
	String product;
	double price;
	double discount;
	int expiration;
	boolean status;
	double finalPrice;

	
	public Coupon()
	{
		provider = "";
		product = "";
		price = 0.0;
		discount = 0.0;
		expiration = 0;
		status = false;
		finalPrice = price - (discount * price);
	}
	
	public Coupon(String prov, String prod, double p, double d, int e, boolean s )
	{
		provider = prov;
		product = prod;
		price = p;
		discount = d;
		expiration = e;
		status = s;
		finalPrice = price - (discount * price);

	}
	
	public String getProvider()
	{
		return provider;
	}
	
	public String getProduct()
	{
		return product;
	}
	
	public double getPrice()
	{
		return price;
	}
	
	public double getFinalPrice()
	{
		return finalPrice;
	}
	
	public double getDiscount()
	{
		return discount;
	}
	
	public int getExpiration()
	{
		return expiration;
	}
	
	public boolean getStatus()
	{
		return status;
	}
	

	
	public void setProvider(String prov)
	{
		provider = prov;
	}
	
	public void setProduct(String prod)
	{
		product = prod;
	}
	
	public void setPrice(double p)
	{
		price = p;
	}
	
	public void setFinalPrice(double fp)
	{
		finalPrice = fp;
	}
	
	public void setDiscount(double d)
	{
		discount = d;
	}
	
	public void setExpiration(int e)
	{
		expiration = e;
	}
	
	public void setStatus(boolean s)
	{
		status = s;
	}
	
	public String toString()
	{
		DecimalFormat df = new DecimalFormat("0.00");
		String out = "\nProvider: "+provider+"\t\t\tProduct: "+product+"\nPrice: $"+df.format(price)+
						"\t\t\t\tDiscount: "+discount*100+"%\nFinal price: $"+df.format(finalPrice)+
						"\t\t\tExpires in: "+expiration+" days\n"+redeemed();
		return out;
	}
	
	public String redeemed()
	{
		if(status == true)
		{
			return "This coupon has been redeemed already.\n";
		}
		else
		{
			return "This coupon has not been used yet.\n";
		}
	}
}