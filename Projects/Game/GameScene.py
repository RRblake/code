import pygame, sys, time, random
from pygame.locals import *
FPS = 60
WINDOWHEIGHT = 480
WINDOWWIDTH = 680
HALFHEIGHT = 240
HALFWIDTH = 340
DISPLAY = (WINDOWWIDTH, WINDOWHEIGHT)
DEPTH = 0
FLAGS = 0

class scene(object):
	def __init__(self):
		pass
	
	def render(self, screen):
		raise NotImplementedError
		
	def update(self):
		raise NotImplementedError
	
	def handle_events(self, events):
		raise NotImplementedError
		

		
class GameScene(scene):
	def __init__(self, x, y):
		super(GameScene, self).__init__()
		self.music = pygame.mixer.music.load('music.mid')
		self.bg = background(WINDOWWIDTH + 1000, WINDOWHEIGHT + 600)
		self.player = Player(x, y)
		self.dude = dude(150, 150)
		self.fire1 = fire(132, 100)
		self.fire2 = fire(200, 200)
		self.entities = pygame.sprite.Group()
		self.camera = Camera(complex_camera, WINDOWWIDTH + 1000, WINDOWHEIGHT + 600)
		self.entities.add(self.player)
		self.blocks = pygame.sprite.Group()
		self.blocks.add(self.dude)
		self.blocks.add(self.fire1)
		self.blocks.add(self.fire2)
		self.music = True
		pygame.mixer.music.play(-1, 0.0)
	def render(self, screen):
		screen.blit(self.bg.image, self.camera.apply(self.bg))
		
		for e in self.entities:
			screen.blit(e.image, self.camera.apply(e))
		for b in self.blocks:
			screen.blit(b.image, self.camera.apply(b))
	def update(self):
		self.camera.update(self.player)
		if self.player.combatStart == True:
			self.manager.go_to(CombatScene(self.player))

		pressed = pygame.key.get_pressed()
		up, left, right, down = [pressed[key] for key in (ord('w'), ord('a'), ord('d'), ord('s'))]
		self.player.update(up, left, right, down, self.blocks)
		self.fire1.update()
		self.fire2.update()
		self.dude.update()
	def handle_events(self, events):
		pass
		for e in events:
			if e.type == KEYDOWN and e.key == K_ESCAPE:
				pygame.mixer.music.stop()
				self.manager.go_to(TitleScene())
			if e.type == KEYDOWN and e.key == ord('i'):
				pygame.mixer.music.stop()
				self.manager.go_to(InvScene(self.player))
			if e.type == KEYDOWN and e.key == K_SPACE and self.player.eventStart:
				self.manager.go_to(EventScene(self.player))
		
class TitleScene(object):
	def __init__(self):
		super(TitleScene, self).__init__()
		self.font = pygame.font.SysFont('Arial', 56)
		self.sfont = pygame.font.SysFont('Arial', 32)
	def render(self, screen):
		screen.fill((64, 64, 64))
		text1 = self.font.render('Luprial The Game', True, (255, 255, 255))
		text2 = self.sfont.render('> press space to start <', True, (255, 255, 255))
		fontrect = text1.get_rect()
		sfontrect = text2.get_rect()
		fontrect.centerx = screen.get_rect().centerx
		fontrect.centery = screen.get_rect().centery
		sfontrect.centerx = fontrect.centerx
		sfontrect.centery = screen.get_rect().centery + 100
		screen.blit(text1, fontrect)
		screen.blit(text2, sfontrect)
	def update(self):
		pass
	def handle_events(self, events):
		for e in events:
			if e.type == KEYDOWN and e.key == K_SPACE:
				self.manager.go_to(GameScene(96, 132))

class EventScene(object):
	def __init__(self, player):
		super(EventScene, self).__init__()
		self.font = pygame.font.SysFont('Arial', 32)
		self.x = player.x
		self.y = player.y
		self.player = player
		self.player.eventStart = False
	def render(self, screen):
		text1 = self.player.eventBlock.event()
		fontrect = text1.get_rect()
		fontrect.centerx = screen.get_rect().centerx
		fontrect.centery = screen.get_rect().centery
		screen.blit(text1, fontrect)
		
	def update(self):
		pass
	def handle_events(self, events):
		for e in events:
			if e.type == KEYDOWN and e.key == K_SPACE:
				self.manager.go_to(GameScene(self.x, self.y))
class CombatScene(object):
	def __init__(self, player):
		super(CombatScene, self).__init__()
		self.font = pygame.font.SysFont('Arial', 56)
		self.sfont = pygame.font.SysFont('Arial', 32)
		self.x = player.x
		self.y = player.y
		self.player = player.combatStart = False
	def render(self, screen):
		screen.fill((0, 255, 0))
		text1 = self.font.render('Combat!', True, (255, 255, 255))
		text2 = self.sfont.render('> press space to return! <', True, (255, 255, 255))
		fontrect = text1.get_rect()
		sfontrect = text2.get_rect()
		fontrect.centerx = screen.get_rect().centerx
		fontrect.centery = screen.get_rect().centery
		sfontrect.centerx = fontrect.centerx
		sfontrect.centery = screen.get_rect().centery + 100
		screen.blit(text1, fontrect)
		screen.blit(text2, sfontrect)
	def update(self):
		pass
	def handle_events(self, events):
		for e in events:
			if e.type == KEYDOWN and e.key == K_SPACE:
				self.manager.go_to(GameScene(self.x, self.y))
class InvScene(object):
	def __init__(self, player):
		super(InvScene, self).__init__()
		self.font = pygame.font.SysFont('Arial', 56)
		self.sfont = pygame.font.SysFont('Arial', 32)
		self.stat = pygame.font.SysFont('Arial', 24)
		self.bfont = pygame.font.SysFont('Arial', 20)
		self.x = player.x
		self.y = player.y
		self.hp = player.hp
		self.attk = player.attk
		self.dfse = player.dfse
		self.magi = player.magi
		self.rest = player.rest
		self.level = player.level
		self.currentXp = player.currentXp
		self.lh = player.leftHand
		
		self.xp = player.xp
		
	def render(self, screen):
		screen.fill((128, 128, 128))
		text1 = self.stat.render('> Inventory <', True, (255, 255, 255))
		text2 = self.stat.render('> Press i to exit <', True, (255, 255, 255))
		text3 = self.stat.render("> Stats <", True, (255,255,255))
		text4 = self.bfont.render("Level: %d" % self.level, True, (255, 255, 255))
		text5 = self.bfont.render("HP: %d" % self.hp, True, (255, 255, 255))
		text6 = self.bfont.render("Attack: %d +%d" % (self.attk, self.lh.attack), True, (255, 255, 255))
		text7 = self.bfont.render("Defense: %d +%d" % (self.dfse, self.lh.defense), True, (255, 255, 255))
		text8 = self.bfont.render("Magic: %d +%d"% (self.magi, self.lh.magic), True, (255, 255, 255))
		text9 = self.bfont.render("Resistence: %d +%d" % (self.rest, self.lh.resist), True, (255, 255, 255))
		text10 = self.bfont.render("XP: %d/%d" % (self.currentXp, self.xp), True, (255, 255, 255))
		text11 = self.stat.render("> Equipment <", True, (255, 255, 255))
		text12 = self.bfont.render("Left Hand: %s" % self.lh.name, True, (255, 255, 255))
		fontrect = text1.get_rect()
		sfontrect = text2.get_rect()
		stats = text3.get_rect()
		l = text4.get_rect()
		h = text5.get_rect()
		a = text6.get_rect()
		d = text7.get_rect()
		m = text8.get_rect()
		r = text9.get_rect()
		x = text10.get_rect()
		equip = text11.get_rect()
		lh = text12.get_rect()
		fontrect.top = 4
		fontrect.right = WINDOWWIDTH - 20
		sfontrect.centerx = screen.get_rect().centerx
		sfontrect.bottom = WINDOWHEIGHT - 4
		stats.top = 4
		stats.left = 20
		l.left = stats.left
		l.top = stats.bottom + 2
		h.left = stats.left
		h.top = l.bottom + 2
		a.left = stats.left
		a.top = h.bottom + 2
		d.left = stats.left
		d.top = a.bottom + 2
		m.left = stats.left
		m.top = d.bottom + 2
		r.left = stats.left
		r.top = m.bottom + 2
		x.left = stats.left
		x.top = r.bottom + 2
		equip.left = stats.left
		equip.top = x.bottom + 8
		lh.left = stats.left
		lh.top = equip.bottom + 4
		screen.blit(text1, fontrect)
		screen.blit(text2, sfontrect)
		screen.blit(text3, stats)
		screen.blit(text4, l)
		screen.blit(text5, h)
		screen.blit(text6, a)
		screen.blit(text7, d)
		screen.blit(text8, m)
		screen.blit(text9, r)
		screen.blit(text10, x)
		screen.blit(text11, equip)
		screen.blit(text12, lh)
	def update(self):
		pass
	def handle_events(self, events):
		for e in events:
			if e.type == KEYDOWN and e.key == ord('i'):
				self.manager.go_to(GameScene(self.x, self.y))
class SceneManager(object):
	def __init__(self):
		self.go_to(TitleScene())
		
	def go_to(self, scene):
		self.scene = scene
		self.scene.manager = self

class Camera(object):
	def __init__(self, camera_func, width, height):
		self.camera_func = camera_func
		self.state = Rect(0, 0, width, height)
	
	def apply(self, target):
		return target.rect.move(self.state.topleft)
	
	def update(self, target):
		self.state = self.camera_func(self.state, target.rect)
		
def simple_camera(camera, target_rect):
	l, t, _, _ = target_rect
	_, _, w, h = camera
	return Rect(-l + HALFWIDTH, -t + HALFHEIGHT, w, h)
	
def complex_camera(camera, target_rect):
	l, t, _, _ = target_rect
	_, _, w, h = camera
	l, t, _, _ = -l + HALFWIDTH, -t + HALFHEIGHT, w, h
	l = min(0, l)
	l = max(-(camera.width - WINDOWWIDTH), l)
	t = max(-(camera.height - WINDOWHEIGHT), t)
	t = min(0, t)
	return Rect(l, t, w, h)
	
def main():
	pygame.init()
	screen = pygame.display.set_mode (DISPLAY)
	timer = pygame.time.Clock()
	running = True
	manager = SceneManager()
	
	while running:
		timer.tick(FPS)
		
		if pygame.event.get(QUIT):
			running = False
			return
		manager.scene.handle_events(pygame.event.get())
		manager.scene.update()
		manager.scene.render(screen)
		pygame.display.flip()
class animate(object):
	def __init__(self, picture):
		self.picture = picture
		self.pic = []
		
	def next_pic(self, pic):
		self.pic = pic
		
class Entity(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)

class Block(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		
class weapons(object):
	def __init__(self):
		pass
		
class smallSword(weapons):
	def __init__(self):
		super(smallSword, self).__init__()
		self.name = "Small Sword"
		self.attack = 1
		self.defense = 0
		self.magic = 0
		self.resist = 0
	def get_attack(self):
		return self.attack

		
class Player(Entity):
	def __init__(self, x, y):
		Entity.__init__(self)
		self.x = x
		self.y = y
		self.xvel = 0
		self.yvel = 0
		self.hp = 10
		self.attk = 2
		self.dfse = 1
		self.magi = 0
		self.rest = 0
		self.currentXp = 0
		self.xp = 10
		self.level = 1
		self.levelUp = False
		self.leftHand = smallSword()
		self.rightHand = None
		self.helm = None
		self.chest = None
		self.legs = None
		self.f = []
		self.f.append(animate(pygame.image.load('Link1.png')))
		self.f.append(animate(pygame.image.load('Link2.png')))
		self.f[0].next_pic(self.f[1])
		self.f[1].next_pic(self.f[0])
		self.b = []
		self.b.append(animate(pygame.image.load('LinkB1.png')))
		self.b.append(animate(pygame.image.load('LinkB2.png')))
		self.b[0].next_pic(self.b[1])
		self.b[1].next_pic(self.b[0])
		self.r = []
		self.r.append(animate(pygame.image.load('LinkR1.png')))
		self.r.append(animate(pygame.image.load('LinkR2.png')))
		self.r[0].next_pic(self.r[1])
		self.r[1].next_pic(self.r[0])
		self.l = []
		self.l.append(animate(pygame.image.load('LinkL1.png')))
		self.l.append(animate(pygame.image.load('LinkL2.png')))
		self.l[0].next_pic(self.l[1])
		self.l[1].next_pic(self.l[0])
		self.currentFront = self.f[0]
		self.currentRight = self.r[0]
		self.currentLeft = self.l[0]
		self.currentBack = self.b[0]
		self.image = self.currentRight.picture
		self.rect = pygame.Rect(x, y, 32, 32)
		self.combat = 1
		self.combatStart = False
		self.ticks = 0
		self.eventTicks = 0
		self.eventStart = False
		self.eventBlock = None
	def update(self, up, left, right, down, blocks):

		chance = random.randint(0,4000)
		if up:
			self.yvel = -3
			if chance <= self.combat:
				self.combatStart = True
			if self.ticks == 5:
				self.currentBack = self.currentBack.pic
				self.ticks = 0
			self.image = self.currentBack.picture
			self.ticks += 1
		if left:
			self.xvel = -3
			if chance <= self.combat:
				self.combatStart = True
			if self.ticks == 5:
				self.currentLeft = self.currentLeft.pic
				self.ticks = 0
			self.image = self.currentLeft.picture
			self.ticks += 1
		if right:
			self.xvel = 3
			if chance <= self.combat:
				self.combatStart = True
			if self.ticks == 5:
				self.currentRight = self.currentRight.pic
				self.ticks = 0
			self.image = self.currentRight.picture
			self.ticks += 1
		if down:
			self.yvel = 3
			if chance <= self.combat:
				self.combatStart = True
			if self.ticks == 5:
				self.currentFront = self.currentFront.pic
				self.ticks = 0
			self.image = self.currentFront.picture
			self.ticks += 1
		if not(left or right):
			self.xvel = 0
		if not(up or down):
			self.yvel = 0
		self.x += self.xvel
		self.y += self.yvel
		self.rect.left += self.xvel
		self.rect.top += self.yvel
		collision = False
		for b in blocks:
			if self.rect.colliderect(b.rect):
				self.eventStart = True
				self.eventBlock = b
				collision = True
				break
		if collision:
			if down or up:
				self.y -= self.yvel
				self.rect.top -= self.yvel	
			if right or left:
				self.x -= self.xvel
				self.rect.left -= self.xvel	
		self.eventTicks += 1
		if self.eventTicks == 5:
			self.eventStart = False
			self.eventTicks = 0
			
class fire(Block):
	def __init__(self, x, y):
		Block.__init__(self)
		self.f = []
		self.f.append(animate(pygame.image.load('fire1.png')))
		self.f.append(animate(pygame.image.load('fire2.png')))
		self.f.append(animate(pygame.image.load('fire4.png')))
		self.f.append(animate(pygame.image.load('fire5.png')))
		self.f[0].next_pic(self.f[1])
		self.f[1].next_pic(self.f[2])
		self.f[2].next_pic(self.f[3])	
		self.f[3].next_pic(self.f[0])
		self.current = self.f[0]
		self.image = self.f[0].picture
		self.rect = pygame.Rect(x, y, 32, 64)
		self.ticks = 0
		
	def update(self):
		if self.ticks == 5:
				self.current = self.current.pic
				self.ticks = 0
		self.image = self.current.picture
		self.ticks += 1
class grass(Entity):
	def __init__(self, x, y):
		Entity.__init__(self)
		self.image = pygame.image.load('Grass.jpg')
		self.rect = pygame.Rect(x,y, 32, 32)

class background(Entity):
	def __init__(self, x, y):
		Entity.__init__(self)
		self.i = pygame.image.load('Grass.jpg')
		self.image = pygame.transform.scale(self.i, (x, y))
		self.rect = pygame.Rect(0, 0 ,x, y)
		
class dude(Block):
	def __init__(self, x, y):
		Block.__init__(self)
		self.font = pygame.font.SysFont('Arial', 20)
		self.f = []
		self.f.append(animate(pygame.image.load('dude1.png')))
		self.f.append(animate(pygame.image.load('dude2.png')))
		self.f.append(animate(pygame.image.load('dude3.png')))
		self.f[0].next_pic(self.f[1])
		self.f[1].next_pic(self.f[2])
		self.f[2].next_pic(self.f[0])
		self.current = self.f[0]
		self.image = self.f[0].picture
		self.rect = pygame.Rect(x, y, 32, 32)
		self.ticks = 0

		
	def update(self):
		if self.ticks == 20:
				self.current = self.current.pic
				self.ticks = 0
		self.image = self.current.picture
		self.ticks += 1
		
	def event(self):
		return self.font.render('Hello!', True, (255, 255, 255))

	"""
	def generateWorld():
	pygame.mixer.music.load('music.mid')
	pygame.mixer.music.play(-1, 0.0)
	musicPlaying = True
	currentFire = fire[0]
	fireTicks = 0
	tiles = []
	f = pygame.Rect(windowSurface.get_rect().centerx, windowSurface.get_rect().centery, 32, 64)
	currentTile = tiles[0]
	player = pygame.Rect(windowSurface.get_rect().centerx - 40, windowSurface.get_rect().centery + 32, 32, 32)
	basic = pygame.Rect(1,1,1,1)
	rock = pygame.image.load('Bigrock.png')
	tree = pygame.image.load('Tree.png')
	trunk = pygame.image.load('trunk.png')
	g1I = pygame.image.load('grass1.png')
	g4I = pygame.image.load('grass4.png')
	key = pygame.image.load('key.png')
	fire1 = pygame.image.load('fire1.png')
	fire2 = pygame.image.load('fire2.png')
	#fire3 = pygame.image.load('fire3.png')
	fire4 = pygame.image.load('fire4.png')
	fire5 = pygame.image.load('fire5.png')
	keyOne = pygame.Rect(100, 100, 12, 21)
	fire1 = pygame.image.load('fire1.png')
	fire2 = pygame.image.load('fire2.png')
	#fire3 = pygame.image.load('fire3.png')
	fire4 = pygame.image.load('fire4.png')
	fire5 = pygame.image.load('fire5.png')
	fire = []
	fire.append(animate("f1", fire1))
	fire.append(animate("f2", fire2))
	#fire.append(animate("f3", fire3))
	fire.append(animate("f4", fire4))
	fire.append(animate("f5", fire5))
	fire[0].next_pic(fire[1])
	fire[1].next_pic(fire[2])
	fire[2].next_pic(fire[3])	
	fire[3].next_pic(fire[0])	
	#fire[4].next_pic(fire[0])
	
	global b1, b2, blocks, rock, grassblocks, g4, t1, t2, b3, b4, b5, rocks, trees
	b1 = pygame.Rect(, random.randint(0, WINDOWHEIGHT - 128), 128, 128)
	b2 = pygame.Rect(random.randint(0, WINDOWWIDTH - 128), random.randint(0, WINDOWHEIGHT - 128), 128, 128)
	b1 = pygame.Rect(, random.randint(0, WINDOWHEIGHT - 128), 128, 128)
	b2 = pygame.Rect(random.randint(0, WINDOWWIDTH - 128), random.randint(0, WINDOWHEIGHT - 128), 128, 128)
	b3 = pygame.Rect(t1.left + 83, t1.bottom-26, 26, 26)
	b4 = pygame.Rect(t2.left + 83, t2.bottom-26, 26, 26)
	blocks = [b1, b2, b3, b4]
	rocks = [b1, b2]
	trees = [t1, t2]
	g4 = pygame.transform.scale(g4I, (40, 40))
	grassblocks = []
	for i in range (20):
		grassblocks.append(pygame.Rect(random.randint(0, WINDOWWIDTH - 40), random.randint(0, WINDOWHEIGHT - 40), 40, 40))
	backgroundImage = pygame.image.load('frostGround.jpg')
	backgroundStretch = pygame.transform.scale(backgroundImage, (WINDOWWIDTH, WINDOWHEIGHT))
	for t in range(4):
		t = tile(t, backgroundStretch, rocks)
		tiles.append(t)
	tiles[0].add_up(tiles[1])
	tiles[0].add_right(tiles[2])
	tiles[0].add_left(None)
	tiles[0].add_down(None)
	tiles[1].add_up(None)
	tiles[1].add_right(tiles[3])
	tiles[1].add_left(None)
	tiles[1].add_down(tiles[0])
	tiles[2].add_up(tiles[3])
	tiles[2].add_right(None)
	tiles[2].add_left(tiles[0])
	tiles[2].add_down(None)
	tiles[3].add_up(None)
	tiles[3].add_right(None)
	tiles[3].add_left(tiles[1])
	tiles[3].add_down(tiles[2])
	tiles[0].add_block(f)
	tiles[3].add_block(keyOne)
	"""
if __name__ == "__main__":
	main()