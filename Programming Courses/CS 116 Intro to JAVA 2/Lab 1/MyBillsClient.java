package Client;
import Client.Services.BillsType;
import Client.Services.Classes.MyBills;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

public class MyBillsClient
{
	
	public String[] totalExpensesPerMonth(MyBills[] exp)
	{
		String [] tepm=new String[4];
		
		String monthNeed="";
		double totaljan=0;
		double totalfeb=0;
		double totalmar=0;
		double totalapr=0;
		
		for(int i=0; i<=exp.length-1; i++)
		{
			monthNeed=exp[i].getMonth();
			if (monthNeed == "JANUARY")
			{
				double [] expen = exp[i].getExpenses();
				for(int e=0; e<=expen.length-1; e++)
				{
					totaljan=totaljan+expen[e];
				}
				tepm[0]="The total of all expenses for the month of "+monthNeed+" is: $"+totaljan;
			}
			else if (monthNeed == "February")
			{
				double [] expen = exp[i].getExpenses();
				for(int e=0; e<=expen.length-1; e++)
				{
					totalfeb=totalfeb+expen[e];
				}
				tepm[1]="The total of all expenses for the month of "+monthNeed+" is: $"+totalfeb;
			}
			else if (monthNeed == "MARCH")
			{
				double [] expen = exp[i].getExpenses();
				for(int e=0; e<=expen.length-1; e++)
				{
					totalmar=totalmar+expen[e];
				}
				tepm[2]="The total of all expenses for the month of "+monthNeed+" is: $"+totalmar;
			}
			else if (monthNeed == "APRIL")
			{
				double [] expen = exp[i].getExpenses();
				for(int e=0; e<=expen.length-1; e++)
				{
					totalapr=totalapr+expen[e];
				}
				tepm[3]="The total of all expenses for the month of "+monthNeed+" is: $"+totalapr;
			}
		}
		return tepm;
	}
	public static void main(String [] args)
	{
		double [] expens=null;
		MyBills[] mbar=null;
				
		try
		{							
			int count1=0;			
			File f=new File("myexpenses.txt");
			Scanner scan1=new Scanner(f);
			while(scan1.hasNextLine())
			{
				String line=scan1.nextLine();	
				count1++;
			}
			mbar=new MyBills[(count1/2)];
			Scanner scan2=new Scanner(f);
			int count3=0;
			while(scan2.hasNextLine())
			{
				int count2=0;
				
				int o=0;
				String line1=scan2.nextLine();
				String line2=scan2.nextLine();
				String line3=line2;
				StringTokenizer stt1=new StringTokenizer(line1,":");
				StringTokenizer stt2=new StringTokenizer(line2,",");
				StringTokenizer stt3=new StringTokenizer(line3,",");
				String garbage=stt1.nextToken();
				String mont=stt1.nextToken();
				garbage=stt1.nextToken();
				String typst=stt1.nextToken();
				BillsType typ = BillsType.valueOf(typst);
				while(stt2.hasMoreTokens())
				{
					String valst=stt2.nextToken();
					Double st=Double.parseDouble(valst);
					count2++;
				}
				expens=new double[count2];
				while(stt3.hasMoreTokens())
				{
					String exst=stt3.nextToken();
					Double ep=Double.parseDouble(exst);
					expens[o]=ep;
					o++;
				}			
				mbar[count3]=new MyBills(mont, typ, expens, expens.length);
								
				count3++;
				
			}
			
		
		
		}
		catch (IOException ioe)
		{		
		  System.out.println("Something went wrong.");
		}
		for(int y=0; y<=mbar.length-1; y++)
		{
			System.out.println(String.valueOf(mbar[y]));
		}
		MyBillsClient mbc=new MyBillsClient();
		String [] Display=new String[4];
		Display=mbc.totalExpensesPerMonth(mbar);
		System.out.println(Display[1]);
		
	}
	

}