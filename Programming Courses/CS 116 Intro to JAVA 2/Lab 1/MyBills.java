package Client.Services.Classes;
import Client.Services.BillsType;
public class MyBills
{
	String month=" ";
	BillsType bt=null;
	double [] expenses;
	int days=0;
	static int ID=0;
	int billsID=0;
	
	public MyBills()
	{
		ID++;
		billsID=ID;
		month="Any Month";
		bt=null;
		expenses=null;
		days=0;
	}
	
	public MyBills(String mon, BillsType enumbt, double [] exp, int da)
	{
		ID++;
		billsID=ID;
		month=mon;
		bt=enumbt;
		expenses=new double [exp.length];
		for(int i=0; i<expenses.length; i++)
		{
			expenses[i]=exp[i];
		}
		days=da;
	}
	
	public int getID()
	{
		return ID;
	}
	
	public int getBillsID()
	{
		return billsID;
	}
	
	public String getMonth()
	{
		return month;
	}
	
	public BillsType getBills()
	{
		return bt;
	}
	
	public double [] getExpenses()
	{
		double [] temp =new double[expenses.length];
		for(int i=0; i<expenses.length; i++)
		{
			temp[i]=expenses[i];
		}
		return temp;
	}
	
	public int getDays()
	{
		return days;
	}
	
	public void setID(int i)
	{
		ID=i;
	}
	
	public void setBillsID(int bid)
	{
		billsID=bid;
	}
	
	public void setMonth(String mo)
	{
		month=mo;
	}
	
	public void setBills(BillsType bity)
	{
		bt=bity;
	}
	
	public void setExpenses(double [] expe)
	{
		expenses=new double [expe.length];
		for(int i=0; i<expenses.length; i++)
		{
			expenses[i]=expe[i];
		}
	}
	
	public void setDays(int d)
	{
		days=d;
	}
	
	public boolean equals(MyBills bi)
	{
		if(this.getBills().equals(bi.getBills())&&this.getDays()==bi.getDays())
			return true;
		else
			return false;
	}
	
	public String toString()
	{
		String b=" ";
		for(int i=0;i<=expenses.length-1;i++)
		{
			double a=expenses[i];
			b=b+" "+String.valueOf(a);
		}
		
		
		String out="The Month is: "+month+", "+"The type of expenses is: "+bt+", "+"The amounts are: "+b+", "+"The number of days are: "+days+", "+"The expense object ID is: "+billsID+", "+"And the static is value is: "+ID+".";
		return out;
	}
		
}