import java.util.*;
import java.io.*;
public class FigureCost
{
	public static void main(String[] args) 
	{
		OutputStreamWriter filestream;
		FileOutputStream file;
		
		ArrayList<Figure> fig1=new ArrayList<Figure>();
		String Line="";
		String Line2="";
		
		try
		{
			file=new FileOutputStream("Output.txt", true);
			filestream=new OutputStreamWriter(new BufferedOutputStream(file));
			System.out.println("Please enter the shape, the length (or radius), and the asked value separated by a space between each value or type done");
			Scanner scan1=new Scanner(System.in);
			Line=scan1.nextLine();
			while(!Line.equals("done"))
			{	
				filestream.write(Line+"\n");

				StringTokenizer tok=new StringTokenizer(Line);
				String sha=tok.nextToken();
				String lens=tok.nextToken();
				double len=Double.parseDouble(lens);
				String val=tok.nextToken();
				if(sha=="Cube")
				{
					Cube cube=new Cube(sha,len,val);
					fig1.add(cube);
				}
				else if (sha=="Sphere")
				{
					Sphere sphere=new Sphere(sha,len,val);
					fig1.add(sphere);
				}
				System.out.println("Please enter the shape, the length (or radius), and the asked value separated by a space between each value or type done");
				scan1=new Scanner(System.in);
				Line=scan1.nextLine();								
			}
			Vector v3=new Vector();
			System.out.println("Please enter information to create customer or type done");
			Scanner scan2=new Scanner(System.in);
			Line2=scan2.nextLine();
			while(!Line2.equals("done"))
			{
				filestream.write(Line2+"\n");
				StringTokenizer tok2=new StringTokenizer(Line2, " ");
				String typ=tok2.nextToken();
				String scos=tok2.nextToken();
				int sco=Integer.parseInt(scos);
				Customers c1=new Customers();
				c1.setType(typ);
				c1.setScore(sco);
				v3.add(c1);
				System.out.println("Please enter information to create customer or type done");
				scan2=new Scanner(System.in);
				Line2=scan2.nextLine();
			}
			filestream.flush();
			file.close();
		}
		catch(IOException IOE)
		{
			System.out.println("Somthing went wrong.");
		}
		for(int j=0; j<fig1.size(); j++)
		{	
			System.out.println(fig1.get(j));
		}
		

	}
}
