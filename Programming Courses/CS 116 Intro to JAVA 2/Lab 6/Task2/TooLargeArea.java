public class TooLargeArea extends Exception
{
	public TooLargeArea()
	{
	
	}
	
	public String getMessage()
	{
		String out1="The area calculated exceeds the limits.";
		return out1;
	}
	
	public String toString()
	{
		String out2="TooLargeArea exception occurred.";
		return out2;
	}
}