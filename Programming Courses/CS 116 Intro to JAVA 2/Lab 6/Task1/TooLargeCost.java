public class TooLargeCost extends Exception
{
	public TooLargeCost()
	{
	
	}
	
	public String getMessage()
	{
		String out1="The cost calculated exceeds the limits.";
		return out1;
	}
	
	public String toString()
	{
		String out2="TooLargeCost exception occurred.";
		return out2;
	}
}