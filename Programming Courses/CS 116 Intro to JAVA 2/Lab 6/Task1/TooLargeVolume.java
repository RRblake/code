public class TooLargeVolume extends Exception
{
	public TooLargeVolume()
	{
	
	}
	
	public String getMessage()
	{
		String out1="The volume calculated exceeds the limits.";
		return out1;
	}
	
	public String toString()
	{
		String out2="TooLargeVolume exception occurred.";
		return out2;
	}
}