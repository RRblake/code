package randy;
import randy.remblake.*;
import java.io.*;
import java.util.*;

public class RodClient
{
	public static void main(String[] args)
	{

		Vector v=new Vector();
		Vector vs=new Vector();
		ReadData rd=new ReadData();
		v=rd.vectorData(args[0]);
		double[] coarr=rd.coar;
		double[] yoarr=rd.yoar;
		vs=rd.sortVector(v);
		for(int i=0; i<vs.size();i++)
		{
			Object obj4=vs.elementAt(i);
			Rod r4=(Rod)obj4;
			double caldl=r4.calculateExpansion(coarr[r4.getRodID()]);
			double force=r4.calculateForce(yoarr[r4.getRodID()], caldl);
			System.out.println(r4.toString());
			System.out.println("The expansion is: "+caldl);
			System.out.println("The Force is: "+force);
		}
		
		int a=rd.searchVector(vs, 89);
		int b=rd.searchVector(vs, 156);
		if(a==-1)
		{
			System.out.println("Temperature not found.");
		}
		else
		{
			Object obj5=vs.elementAt(a);
			Rod r5=(Rod)obj5;
			System.out.println("The object was found and it is: "+r5.toString());
		}
		
		if(b==-1)
		{
			System.out.println("Temperature not found.");
		}
		else
		{
			Object obj6=vs.elementAt(b);
			Rod r6=(Rod)obj6;
			System.out.println("The object was found and it is: "+r6.toString());
		}
	}
}