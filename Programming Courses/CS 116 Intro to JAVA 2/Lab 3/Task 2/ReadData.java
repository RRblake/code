package randy.remblake;
import java.util.*;
import java.io.*;
import randy.remblake.*;

public class ReadData
{
	public double[] coar=null;
	public double[] yoar=null;
	
	public Vector vectorData(String filename)
	{
		Vector v=new Vector();
		try
		{
			File f=new File(filename);
			Scanner scan1=new Scanner(f);
			int index1=0;
			while (scan1.hasNextLine())
			{
				String line1=scan1.nextLine();
				index1++;
			}
			coar=new double[index1];
			yoar=new double[index1];
			Scanner scan2=new Scanner(f);
			int index2=0;
			while (scan2.hasNextLine())
			{
				String line2=scan2.nextLine();
				StringTokenizer stt=new StringTokenizer(line2,",");
				String strmats=stt.nextToken();
				MaterialCategories mats=MaterialCategories.valueOf(strmats);
				String nam=stt.nextToken();
				String ares=stt.nextToken();
				double are=Double.parseDouble(ares);
				String lens=stt.nextToken();
				double len=Double.parseDouble(lens);
				String dtems=stt.nextToken();
				double dtem=Double.parseDouble(dtems);
				String yons=stt.nextToken();
				double yon=Double.parseDouble(yons);
				String temps=stt.nextToken();
				int temp=Integer.parseInt(temps);
				coar[index2]=dtem;
				yoar[index2]=yon;
				Rod r=new Rod(temp, nam, are, len);
				r.setMat(mats);
				v.addElement(r);
				index2++;
			}
		}
		catch (IOException ioe)
		{
			System.out.println("Something went wrong.");
		}
		return v;
	}
	
	public Vector sortVector(Vector V)
	{
		Vector vsort=new Vector();
		
		int index4=V.size();
		for (int j=0; j<index4-1; j++)
		{	
			int index3=0;
			int veclen=V.size();
			for (int k=0; k<veclen; k++)
			{
				Object obj1=V.elementAt(k);
				Rod r1=(Rod)obj1;
				Object obj2=V.elementAt(index3);
				Rod r2=(Rod)obj2;
				if(r1.getTemp()>r2.getTemp())
				{
					index3=k;
				}
				
			}
			vsort.addElement(V.elementAt(index3));
			V.remove(index3);
		}
		return vsort;
	}
	
	public int searchVector(Vector sv, int key)
	{
		int start=0, end=sv.size()-1;
		while(end>=start)
		{
			int middle=(end+start)/2;
			Object obj3=sv.elementAt(middle);
			Rod r3=(Rod)obj3;
			if(r3.getTemp()==key)
			{
				return middle;
			}
			else if(r3.getTemp()>key)
			{
				start=middle+1;
			}
			else
			{
				end=middle-1;
			}
		}
		return -1;
	}
}