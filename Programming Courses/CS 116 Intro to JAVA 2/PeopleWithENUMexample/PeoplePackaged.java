
//George Koutsogiannakis
//September 2008
package george.kay;
public class PeoplePackaged 
{
	String firstName=" ";
	String lastName=" ";
	int socialSecurity=0;
	double height=0.0;
	static int id=0;
	int personID=0;

	//enum variable!!
	PeoplePackagedTypes ppt=null;

	public PeoplePackaged()
	{
		id++;
		personID=id;
		firstName="AnyName";
		lastName="AnyName";
		socialSecurity=123456789;
		height=5.8;
		ppt=PeoplePackagedTypes.CHILD_FEMALE;
	}
	
	public PeoplePackaged(String fn, String ln, int ssn, double ht, PeoplePackagedTypes pt)
	{

		id++;
		personID=id;
		firstName=fn;
		lastName=ln;
		socialSecurity=ssn;
		height=ht;
		ppt=pt;
	}

	public int getID()
	{

		return id;

	}
	
	public int getPersonID()
	{


		return personID;

	}
	
	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public int getSocialSecurity()
	{
		return socialSecurity;
	}

	public double getHeight()
	{
		return height;
	}

	public PeoplePackagedTypes getPeopleTypes()
	{
		return ppt;
	}
	
	
	
	
	public void setID(int i)
	{

		id=i;

	}

	public void setPersonID(int pid)
	{

		personID=pid;
		
	}
	
	
	public void setFirstName(String fina)
	{
		firstName=fina;
	}

	public void setLastName(String lana)
	{
		lastName=lana;
	}

	public void setSocialSecurity(int sosec)
	{
		socialSecurity=sosec;
	}

	public void setHeight(double hei)
	{
		height=hei;
	}


	
	public String toString()
	{


		String output="First Name="+" "+firstName+" "+"Last Name="+" "+lastName+" "+"Social Sec="+" "+socialSecurity+" "+"Height="+" "+
		+height+" "+"id="+" "+id+" "+"Person ID="+personID+" "+"The type is:"+" "+ppt;
		return output;
	}
}



