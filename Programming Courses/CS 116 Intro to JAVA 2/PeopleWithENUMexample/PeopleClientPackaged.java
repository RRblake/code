//George Koutsogiannakis
//Fall 2009

package george;

import javax.swing.JOptionPane;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;
import george.kay.PeoplePackaged;
import george.kay.PeoplePackagedTypes;
public class PeopleClientPackaged  
{
	public static void main(String[] args) 
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter the first name:");
		String fn1=scan.next();
		System.out.println("Please enter the last name:");
		String ln1=scan.next();
		System.out.println("Please enter the social security number:");
		int ssn1=scan.nextInt();
		System.out.println("Please enter the height");

		double hei1=scan.nextDouble();

		System.out.println("Please enter the people type");
		String peotstr=scan.next();
		PeoplePackagedTypes peot=PeoplePackagedTypes.valueOf(peotstr);
		PeoplePackaged person1=new PeoplePackaged(fn1,ln1,ssn1,hei1,peot);
		String fn2=args[0];
		String ln2=args[1];
		String ssn2str=args[2];
		int ssn2=Integer.parseInt(ssn2str);
		String hei2str=args[3];
		
		double hei2=Double.parseDouble(hei2str);

		String peotypestr=args[4];
		PeoplePackagedTypes peotype=PeoplePackagedTypes.valueOf(peotypestr);

		//System.out.println("fn="+fn2+"ln="+ln2+"ssn="+ssn2+"hei="+hei2);
		PeoplePackaged person2=new PeoplePackaged(fn2, ln2, ssn2, hei2, peotype);
		
		String fn3=JOptionPane.showInputDialog(null, "Input from User", "Enter the first name", JOptionPane.INFORMATION_MESSAGE);
		
		String ln3=JOptionPane.showInputDialog(null, "Information", "Enter the last name", JOptionPane.INFORMATION_MESSAGE);
		String ssn3str=JOptionPane.showInputDialog(null, "Information", "Enter the social sec. num", JOptionPane.INFORMATION_MESSAGE);
		int ssn3=Integer.parseInt(ssn3str);
		String hei3str=JOptionPane.showInputDialog(null, "Information", "Enter the height", JOptionPane.INFORMATION_MESSAGE);
		double hei3=Double.parseDouble(hei3str);
		String ptstr=JOptionPane.showInputDialog(null, "Information", "Enter the type", JOptionPane.INFORMATION_MESSAGE);
		PeoplePackagedTypes pt=PeoplePackagedTypes.valueOf(ptstr);
		
		PeoplePackaged person3=new PeoplePackaged(fn3, ln3, ssn3, hei3, pt);
		PeoplePackaged person4=new PeoplePackaged();
		int id3=person3.getID();
		int pid3=person3.getPersonID();
		JOptionPane.showMessageDialog(null, "person3 id="+" "+id3+", "+"person3 personID="+" "+pid3);
		System.out.println(person1.toString());
		System.out.println(person2.toString());
		System.out.println(person3.toString());
		System.out.println(person4.toString());
		person1.setID(10);
		System.out.println(person1.toString());
		System.out.println(person2.toString());
		System.out.println(person3.toString());
		System.out.println(person4.toString());

		//NEW CODE TO READ DATA FROM TEXT FILE data.txt

		try
		{
			File f=new File("data.txt");
			Scanner scan1=new Scanner(f);
			while(scan1.hasNextLine())
			{
				
				
				
				String line=scan1.nextLine();
				StringTokenizer stt=new StringTokenizer(line);
				String fn=stt.nextToken();
				String ln=stt.nextToken();
				String ssns=stt.nextToken();
				int ssn=Integer.parseInt(ssns);
				String hts=stt.nextToken();
				double ht=Double.parseDouble(hts);
				String ptypestr=stt.nextToken();
				PeoplePackagedTypes ptype=PeoplePackagedTypes.valueOf(ptypestr);
				PeoplePackaged p=new PeoplePackaged(fn,ln,ssn,ht,ptype);
				System.out.println(p.toString());

		
			}
		
		
		
		}
		catch (IOException ioe)
		{
		
		  System.out.println(ioe.getMessage());
		}




	}
}

/* OUTPUT

C:\CS116\Lectures\Lecture1REVIEWPart2>java PeopleClientTextFile George Kay 6789 5.9
Please enter the first name:
Phyllis
Please enter the last name:
Zim
Please enter the social security number:
3456
Please enter the height
5.3
First Name= Phyllis Last Name= Zim Social Sec= 3456 Height= 5.3 id= 4 Person ID=1
First Name= George Last Name= Kay Social Sec= 6789 Height= 5.9 id= 4 Person ID=2
First Name= Anna Last Name= Kay Social Sec= 2345 Height= 5.6 id= 4 Person ID=3
First Name= AnyName Last Name= AnyName Social Sec= 123456789 Height= 5.8 id= 4 Person ID=4
First Name= Phyllis Last Name= Zim Social Sec= 3456 Height= 5.3 id= 10 Person ID=1
First Name= George Last Name= Kay Social Sec= 6789 Height= 5.9 id= 10 Person ID=2
First Name= Anna Last Name= Kay Social Sec= 2345 Height= 5.6 id= 10 Person ID=3
First Name= AnyName Last Name= AnyName Social Sec= 123456789 Height= 5.8 id= 10 Person ID=4
First Name= Mary Last Name= Jones Social Sec= 1234 Height= 5.5 id= 11 Person ID=11
First Name= David Last Name= Brown Social Sec= 5678 Height= 5.9 id= 12 Person ID=12
First Name= Nick Last Name= James Social Sec= 9123 Height= 5.8 id= 13 Person ID=13

*/
