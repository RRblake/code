import java.util.*;
import java.io.*;
public class FibNumbers
{
	public static void main(String [] args)
	{
		for(int i=0;i<=10;i++)
		{
			System.out.println("The Fibonacci of: "+i+" is "+Fibonacci(i));
		}
	}
	
	public static int Fibonacci(int j)
	{
		if(j==0)
		{
			return 0;
		}
		else if((j==1)||(j==2))
		{
			return 1;
		}
		int fib1=1;
		int fib2=1;
		int fibNew=1;
		for(int k=3;k<=j;k++)
		{
			fibNew=fib2+fib1;
			fib1=fib2;
			fib2=fibNew;
		}
		return fibNew;
	}
}