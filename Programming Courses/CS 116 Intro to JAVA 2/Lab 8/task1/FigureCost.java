import java.util.*;
import java.io.*;
public class FigureCost
{
	public static void main(String[] args) 
	{
		
		ArrayList<Figure> list=new ArrayList<Figure>();
		Vector v1=new Vector();
		INT3 vTemp=new INT3();
		v1=vTemp.secretData();
		
		Scanner scan1=new Scanner(System.in);
		System.out.println("Please enter the shape the length(or radius) and the asked value separated by space or type done");
		String line=scan1.nextLine();
		while(!line.equals("done"))
		{
			StringTokenizer strtok1=new StringTokenizer(line);
			String sha=strtok1.nextToken();
			String length=strtok1.nextToken();
			double len=Double.parseDouble(length);
			String ask=strtok1.nextToken();
			if(sha.equals("cube"))
			{
				Cube cube=new Cube(sha, len, ask);
				list.add(cube);
			}
			else if(sha.equals("sphere"))
			{
				Sphere sphere=new Sphere(sha,len,ask);
				list.add(sphere);
			}
			System.out.println("Please enter the shape the length(or radius) and the asked (area or volume) separated by space or type done");
			line=scan1.nextLine();
		}
		System.out.println(list.size());
		
		ArrayList<String> costslist=new ArrayList<String>();
		FileOutputStream fileFig;
		FileOutputStream fileCost;
		FileOutputStream file;
		ObjectOutputStream oosF;
		ObjectOutputStream oosCo;
		String filename1="Figures.ser";
		String filename2="Customers.ser";
		String filename3="Cost.ser";
        try
        {
        	fileFig = new FileOutputStream(filename1);
			oosF = new ObjectOutputStream(fileFig);
			fileCost = new FileOutputStream(filename3);
			oosCo = new ObjectOutputStream(fileCost);
       
				for(int i=0; i<list.size(); i++)
				{				 
				   try
				   {					
						System.out.println(i);
				        Figure fig=list.get(i);							
						costslist=fig.costToDraw();
						System.out.println("Calculating the cost for shape object:"+" "+list.get(i).toString());
						oosF.writeObject("Calculating the cost for shape object: ");
						oosF.writeObject(list.get(i));
																			
					}
					catch (TooLargeCostException tlc)
					{
						System.out.println(tlc.getMessage());
					   
					}
					for(int j=0; j<costslist.size(); j++)
					{
						System.out.println(costslist.get(j));
						oosCo.writeObject(costslist.get(j));
						oosCo.writeObject("\n");
					}
				}
				
				file.close();
			}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		System.out.println("Type read if you want to have the Figure file read and displayed or type no if you want the rogram terminated");
		Scanner scan2=new Scanner(System.in);
		String str=scan2.next();
		if(str.equals("read"))
		{
		   try
			{
				FileInputStream fis1=new FileInputStream(filename1);
				ObjectInputStream ois1=new ObjectInputStream(fis1); 
				Figure fig1=(Figure)ois1.readObject();				
										
				while((reading=ois1.readLine())!=null) 
				{									 
					System.out.println("Calculating the cost for shape object: "+fig1.toString());									
				}			
				ois1.close();
			}//   end of try
			catch(FileNotFoundException fnf)
			{
				System.out.println("The file was not found");
			}
			catch(IOException ioe)
			{
					System.out.println(ioe.toString()); 
			}
			
		}
		else
		{
			System.out.println("The program will exit without reading the file");
		}
		
		System.out.println("Type read if you want to have teh Customer file read and displayed or type no if you want the rogram terminated");
		Scanner scan2=new Scanner(System.in);
		String str=scan2.next();
		if(str.equals("read"))
		{
		   try
			{
				FileInputStream fis2=new FileInputStream(filename1);
				ObjectInputStream ois2=new ObjectInputStream(fis1); 
				Customers cust1=(Customers)ois2.readObject();				
										
				while((reading=ois2.readLine())!=null) 
				{									 
					System.out.println("The customer is "+cust1.toString());									
				}			
				ois2.close();
			}//   end of try
			catch(FileNotFoundException fnf)
			{
				System.out.println("The file was not found");
			}
			catch(IOException ioe)
			{
					System.out.println(ioe.toString()); 
			}
			
		}
		else
		{
			System.out.println("The program will exit without reading the file");
		}
		
		System.out.println("Type read if you want to have teh Costs file read and displayed or type no if you want the rogram terminated");
		Scanner scan2=new Scanner(System.in);
		String str=scan2.next();
		if(str.equals("read"))
		{
		   try
			{
				FileInputStream fis3=new FileInputStream(filename3);
				ObjectInputStream ois3=new ObjectInputStream(fis3); 
				String cost1=(String)ois3.readObject();				
										
				while((reading=ois3.readLine())!=null) 
				{									 
					System.out.println(cost1);									
				}			
				ois3.close();
			}//   end of try
			catch(FileNotFoundException fnf)
			{
				System.out.println("The file was not found");
			}
			catch(IOException ioe)
			{
					System.out.println(ioe.toString()); 
			}
			
		}
		else
		{
			System.out.println("The program will exit without reading the file");  
		}
                 
				

		   
		
		
		
		
		
	

	}//end of main method
}
