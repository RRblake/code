import java.util.*;
import java.io.*;
public class  Customers implements Serializable
{
	
	public String type=" ";
	public int score=0;

	public void setType(String ty)
	{
		type=ty;
	}

	public void setScore(int sc)
    {
		score=sc;
	}

	public String getType()
	{
		return type;
	}

	public int getScore()
	{
		return score;
	}
	
	public Vector customerData()
	{
		Vector v=new Vector();
		
		Scanner scan=new Scanner(System.in);
		System.out.println("Please enter the 2 pieces of information for type and score for a customer object separated by space or type done");
		String str=scan.nextLine();
		FileOutputStream fileCust;
		ObjectOutputStream oosCu;
		String filename2="Customers.ser";
		try
		{
			fileCust = new FileOutputStream(filename2);
			oosCu = new ObjectOutputStream(fileCust);
			while(!str.equals("done"))
			{
				
				Customers c1=new Customers();
				StringTokenizer strtok=new StringTokenizer(str);
				String type=strtok.nextToken();
				c1.setType(type);
				String score=strtok.nextToken();
				int sc=Integer.parseInt(score);
				c1.setScore(sc);
				oosCu.writeObject(c1);
				oosCu.writeObject("\n");
				v.add(c1);
				System.out.println("Please enter the information for type and score for a customer object separated by space or type done");
				str=scan.nextLine();
			}
			
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
		}
		return v;
	}

	public String toString()
	{
		return "type="+" "+type+" "+"score="+" "+score;
	}
}
