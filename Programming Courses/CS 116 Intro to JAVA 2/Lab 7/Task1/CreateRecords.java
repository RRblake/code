package Compiled;
import java.util.*;
import java.io.*;
public class CreateRecords
{
	public static void main(String [] args)
	{
		BankAccounts ba1=new BankAccounts("1234",'A','M',1000.30,true,"Mypassword");
		BankAccounts ba2=new BankAccounts("3456",'G','L',300.34,false,"helenB");
		BankAccounts ba3=new BankAccounts("7890",'H','J',1290.00,true,"jwer");
		BankAccounts ba4=new BankAccounts("6781",'F','D',260.60,true,"hgfdw");
		try
		{
			File file = new File("records.dat");
			FileOutputStream fileOutput = new FileOutputStream(file);	
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOutput);
			objectOut.writeObject(ba1);
			objectOut.writeObject(ba2);
			objectOut.writeObject(ba3);
			objectOut.writeObject(ba4);
			fileOutput.close();
		}
		catch(IOException IOE)
		{
			System.out.println("IO exception = " + IOE);
		}
		System.out.println("If you would like to read the data from records.dat type yes.");
		Scanner scan1 =new Scanner(System.in);
		String Line =scan1.nextLine();
		if(line=="yes")
		{
			try
			{
				FileInputStream  fis = new FileInputStream(file);
				ObjectInputStream  ois = new ObjectInputStream(fis);
				BankAccounts ba5 = (BankAccounts)ois.readObject();
				BankAccounts ba6 = (BankAccounts)ois.readObject();
				BankAccounts ba7 = (BankAccounts)ois.readObject();
				BankAccounts ba8 = (BankAccounts)ois.readObject();
				ois.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error writing into file");
				ioe.printStackTrace();
			}
		}
		else
		{
			System.out.println("File will not be read, Closing program.");
		}
		

	}
}