package Compiled;
import java.io.*;
import java.util.*;
public class BankAccounts implements Serializable
{
	String bankAccountName;
	char firstName;
	char lastName;
	double balance;
	boolean feesApply;
	String password;
	
	public BankAccounts()
	{
		bankAccountName=" ";
		firstName=' ';
		lastName=' ';
		balance=0.0;
		feesApply=false;
		password=" ";
	}
	
	public BankAccounts(String ba, char fn, char ln, double b, boolean f, String p)
	{
		bankAccountName=ba;
		firstName=fn;
		lastName=ln;
		balance=b;
		feesApply=f;
		password=p;
	}
	
	public String getBankAccountName()
	{
		return bankAccountName;
	}
	
	public char getFirstName()
	{
		return firstName;
	}
	
	public char getLastName()
	{
		return lastName;
	}
	
	public double getBalance()
	{
		return balance;
	}
	
	public boolean getFeesApply()
	{
		return feesApply;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setBankAccountName(String ba2)
	{
		bankAccountName=ba2;
	}
	
	public void setFirstName(char fn2)
	{
		firstName=fn2;
	}
	
	public void setLastName(char ln2)
	{
		lastName=ln2;
	}
	
	public void setBalance(double b2)
	{
		balance=b2;
	}
	
	public void setFeesApply(boolean f2)
	{
		feesApply=f2;
	}
	
	public void setPassword(String p2)
	{
		password=p2;
	}
	
	public String toString()
	{
		String out="The bank account name is "+bankAccountName+" The first initial is "+firstName+" The last name initial is "+lastName+" The balance is "+balance+" The fees apply is true "+feesApply+" The password is "+password;
		return out;
	}
}