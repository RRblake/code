 package Compiled;
import java.util.*;
import java.io.*;
public class CreateRecords
{
	public static void main(String [] args)
	{
		BankAccounts ba1=new BankAccounts("1234",'A','M',1000.30,true,"Mypassword");
		BankAccounts ba2=new BankAccounts("3456",'G','L',300.34,false,"helenB");
		BankAccounts ba3=new BankAccounts("7890",'H','J',1290.00,true,"jwer");
		BankAccounts ba4=new BankAccounts("6781",'F','D',260.60,true,"hgfdw");
		ArrayList<BankAccounts> baList=new ArrayList<BankAccounts>(4);
		baList.add(ba1);
		baList.add(ba2);
		baList.add(ba3);
		baList.add(ba4);
		String file="data.txt";
		try
		{
			
			FileOutputStream fileOutput = new FileOutputStream(file,true);	
			OutputStreamWriter fileStream = new OutputStreamWriter(new BufferedOutputStream(fileOutput));
			
			for(int i=0;i<=baList.size()-1;i++)
			{
				fileStream.write(baList.get(i).getBankAccountName());
				fileStream.write(" ");
				fileStream.write(baList.get(i).getFirstName());
				fileStream.write(" ");
				fileStream.write(baList.get(i).getLastName());
				fileStream.write(" ");
				double temp=baList.get(i).getBalance();
				String tempstr=String.valueOf(temp);
				fileStream.write(tempstr);
				fileStream.write(" ");
				boolean tempbool=baList.get(i).getFeesApply();
				String tempstr2=String.valueOf(tempbool);
				fileStream.write(tempstr2);
				fileStream.write(" ");
				fileStream.write(baList.get(i).getPassword());
				fileStream.write("\n");
			}
			
			fileStream.flush();
			fileOutput.close();
		}
		catch(IOException IOE)
		{
			System.out.println("IO exception = " + IOE);
		}
		System.out.println("If you would like to read the data from data.txt type yes.");
		Scanner scan1 =new Scanner(System.in);
		String Line =scan1.nextLine();
		if(Line.equals("yes"))
		{
			try
			{
				FileInputStream  fis = new FileInputStream(file);
				BufferedReader  fileRead = new BufferedReader(new InputStreamReader(fis));
				String reading="";
				while((reading=fileRead.readLine())!=null)
				{
					System.out.println(reading);
				}
				fis.close();
				fileRead.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error writing into file");
				ioe.printStackTrace();
			}

		}
		else
		{
			System.out.println("File will not be read, Closing program.");
		}
		

	}
}