//George Koutsogiannakis
package main;
import java.util.*;
import java.util.StringTokenizer;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.ArrayList;
import main.service.*;
import main.service.enums.*;

public class AutoClient
{
	public static void main(String[] args) 
	{
		AutoClient ac=new AutoClient();
		AutoStore[] tempar1=storeData();
		Auto[] tempar2=ac.carData();
		ArrayList<Auto> alAuto=new ArrayList<Auto>(tempar2.length);
		System.out.println("-------------FIRST OUTPUT ----------------");
		
		
		for(int i=0;i<=tempar1.length-1;i++)
		{	
			tempar1[i].setAuto(tempar2[i]);
			tempar2[i].setAutoStore(tempar1[i]);
			alAuto.add(tempar2[i]);

		}
		
		for(Auto firstOutput : alAuto)
		{
			System.out.println(firstOutput.toString());
		}

		System.out.println("-------------SECOND OUTPUT----------------");
	
		double[][][] secondOutArray=ac.storeSales(alAuto);
		System.out.println("\n\tMON\tTUE\tWED\tTHU\tFRI\tSAT\n\t---\t---\t---\t---\t---\t---\n");
		for(int j=0;j<=secondOutArray.length-1;j++)
		{
			System.out.print("\nID"+j+"\n---");
			for(int k=0;k<=secondOutArray[j].length-1;k++)
			{	
				System.out.print("\nWeek"+k+"\t");
				for(int h=0;h<=secondOutArray[j][k].length-1;h++)
				{
					System.out.print(secondOutArray[j][k][h]+"    ");
				}
			}
		}
		
		System.out.println("\n-------------THIRD OUTPUT----------------");
	    ArrayList<Auto> soAlAu=new ArrayList<Auto>(alAuto.size());
		soAlAu=ac.sortAutoList(alAuto);
		for(int l=0;l<=soAlAu.size()-1;l++)
		{
			System.out.println(soAlAu.get(l).toString());
		}
		System.out.println("test");
	}
	

	///////////////END OF MAIN METHOD //////////////////////////////////////////

	public Auto[] carData()
	{
		
		Auto [] veharray=null;
		//double[] varray=null;
		//double[] darray=null;
		//double[] farray=null;
		int count=0;
		try
		{
			File file =new File("data.txt");
			Scanner scan=new Scanner(file);
			CarType vehe=null;
			while(scan.hasNextLine())
			{
				scan.nextLine();
				count++;
			}

			veharray=new Auto[count];
			//varray=new double[count];
			//darray=new double[count];
			//farray=new double[count];
			Scanner scan1=new Scanner(file);
			int index=0;
			while(scan1.hasNextLine())
			{
				String line=scan1.nextLine();
				StringTokenizer strtok=new StringTokenizer(line,"#");
				String tok1=strtok.nextToken();
				
				double a=Double.parseDouble(tok1);
				String tok2=strtok.nextToken();

				int t=Integer.parseInt(tok2);
				String tok3=strtok.nextToken();

				String tok4=strtok.nextToken();

				if(tok4.equals("Type1"))
				{
					
					vehe=CarType.Type1;
				}
				else if(tok4.equals("Type2"))
				{
					
					vehe=CarType.Type2;
				}
				else if(tok4.equals("Type3"))
				{
				   vehe=CarType.Type3;
				}
				else if(tok4.equals("Type4"))
				{
					vehe=CarType.Type4;
				}

				String garb1=strtok.nextToken();
				garb1=strtok.nextToken();
				
				Auto v=new Auto(a,t,tok3,vehe);
				veharray[index]=v;
				
				index++;
			}
		}//end of try
		catch(IOException ioe)
		{
		   System.out.println("Something is wrong");
		}
		return veharray;
	}

	public static AutoStore[] storeData()
	{
		AutoStore[] dealarray=null;
		Stores d=null;
		int count=0;
		try
		{
			File file =new File("data.txt");
			Scanner scan=new Scanner(file);
			CarType vehe=null;
			while(scan.hasNextLine())
			{
				scan.nextLine();
				count++;
			}

			dealarray=new AutoStore[count];
			
			Scanner scan1=new Scanner(file);
			int index=0;
			while(scan1.hasNextLine())
			{
				String line2=scan1.nextLine();
				StringTokenizer strtok2=new StringTokenizer(line2,"#");
				String garb2=strtok2.nextToken();
				garb2=strtok2.nextToken();
				garb2=strtok2.nextToken();
				garb2=strtok2.nextToken();
				garb2=strtok2.nextToken();
				String salestr=strtok2.nextToken();
				double saledo=Double.parseDouble(salestr);
				AutoStore aust=new AutoStore(saledo);
				dealarray[index]=aust;
				index++;
			}
		}
		catch(IOException ioe)
		{
		   System.out.println("Something is wrong");
		}
		
		
		return dealarray;
	}

	
	

	public ArrayList<Auto> sortAutoList(ArrayList<Auto> ald)
	{
		ArrayList<Auto> aldsort=new ArrayList<Auto>(ald.size());
		ArrayList<Auto> ald1=new ArrayList<Auto>(ald.size());
		for(int a=0;a<=ald.size()-1;a++)
		{
			ald1.add(ald.get(a));
		}
		int index1=0;
		int index2=0;
		int length1=ald1.size();
		for(int j=0; j<length1-1;j++)
		{
			int length2=length1-j-1;
			for(int k=0; k<=length2;k++)
			{
				if(ald1.get(k).getAutoStore().getSale()<=ald1.get(index1).getAutoStore().getSale())
				{
					index1=k;
				}
				else{}
				if(ald1.get(k).getAutoStore().getSale()>ald1.get(index2).getAutoStore().getSale())
				{
					index2=k;
				}
				else{}
			}
			aldsort.add(ald.get(index1));
			ald1.remove(index1);
			ald1.add(index1,ald.get(index2));
		}
		return aldsort;
	}

	
	public double[][][] storeSales(ArrayList<Auto> al)
	{
		double[][][] sales=new double[5][2][6];
		int countw1=0;
		int countw2=15;
		for(int i=0;i<=sales.length-1;i++)
		{	
			for(int j=0;j<=sales[i].length-1;j++)
			{
				for(int k=0;k<=sales[i][j].length-1;k++)
				{
					if((i==0||i==2||i==4)&&(k==0||k==2||k==4))
					{
						if(j==0)
						{
							sales[i][j][k]=sales[i][j][k]+al.get(countw1).getAutoStore().getSale();
							al.get(countw1).setSold(true);
							countw1++;
							
						}
						else
						{
							sales[i][j][k]=sales[i][j][k]+al.get(countw2).getAutoStore().getSale();
							al.get(countw2).setSold(true);
							countw2++;
						}
					}
					else if((i==1||i==3)&&(k==1||k==3||k==5))
					{
						if(j==0)
						{
							sales[i][j][k]=sales[i][j][k]+al.get(countw1).getAutoStore().getSale();
							al.get(countw1).setSold(true);
							countw1++;
							
						}
						else
						{
							sales[i][j][k]=sales[i][j][k]+al.get(countw2).getAutoStore().getSale();
							al.get(countw2).setSold(true);
							countw2++;
						}
					}
					else{}
					
				}
			}
		}
		return sales;
	}
	
}
