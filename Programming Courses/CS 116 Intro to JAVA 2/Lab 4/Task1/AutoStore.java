
package main.service;

import main.service.enums.*;
import java.lang.*;

public class AutoStore
{
	Stores store=null; 
	private Auto autoObject;
	
	double sale=0.0; 
	static int id=0;
	int currentID=0;


	public AutoStore()
	{
		sale=0.0;
		store=null;

		id++;
		currentID=id;
	}

	public AutoStore(double s)
	{
		sale=s;
		store=null;

		id++;
		currentID=id;
	}

	public Stores getStore()
	{
		return store;
	}

	public double getSale()
	{
		return sale;
	}

	public Auto getAuto()
	{
		return autoObject;
	}


	public int getCurrentID()
	{
		return currentID;
	}

	public void setStore(Stores sto)
	{
		store=sto;
	}

	public void setSale(double sa)
	{
		sale=sa;
	}

	public void setAuto(Auto au)
	{
		autoObject=au;
	}

	public void setCurrentID(int cid)
	{
		currentID=cid;
	}

	public String toString()
	{
		String out="The store is: "+" "+store+" "+"The sale price of the car is: "+" "+sale+" "+"The car is: "+autoObject.getCarType()+" "+"The auto store object id is: "+currentID+"The auto is";
		return out;
	}
}
