package Client.Services;
import Client.Services.Enums.*;
public class Management extends Worker
{
	private double weeklyBenefits;
	private double bonus;
	
	public Management()
	{
		super();
		weeklyBenefits=10.00;
		bonus=10.00;
	}
	
	public Management(String n8, int ss8, int ye8, Jobs j8, double wb3, double b)
	{
		super(n8, ss8, ye8, j8);
		weeklyBenefits=wb3;
		bonus=b;
	}
	
	public double getWeeklyBenefits()
	{
		return weeklyBenefits;
	}
	
	public double getBonus()
	{
		return bonus;
	}
	
	public void setWeeklyBenefits(double wb4)
	{
		weeklyBenefits=wb4;
	}
	
	public void setBonus(double b2)
	{
		bonus=b2;
	}
	
	public String toString()
	{
		String out=super.toString()+" The weekly benefits are "+weeklyBenefits+" The bonus is "+bonus;
		return out;
	}
	
	public double benefitsCalculation(Jobs j10)
	{
		double benefits=0.00;
		if(j10.equals(Jobs.ENGINEERING_MANAGER))
		{
			benefits=weeklyBenefits+bonus;
			return benefits;
		}
		else if(j10.equals(Jobs.ADMINISTRATIVE_MANAGER))
		{
			benefits=weeklyBenefits+bonus*.5;
			return benefits;
		}
		else
		{
			return benefits;
		}
	}
	
}