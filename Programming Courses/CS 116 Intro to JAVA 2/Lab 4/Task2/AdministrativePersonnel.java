package Client.Services;
import Client.Services.Enums.*;
public class AdministrativePersonnel extends Worker
{
	private double rate;
	private double hours;
	
	public AdministrativePersonnel()
	{
		super();
		rate=10.00;
		hours=10.00;
	}
	
	public AdministrativePersonnel(String n5, int ss5, int ye5, Jobs j5, double r, double h)
	{
		super(n5, ss5, ye5, j5);
		rate=r;
		hours=h;
	}
	
	public double getRate()
	{
		return rate;
	}
	
	public double gethours()
	{
		return hours;
	}
	
	public void setRate(double r2)
	{
		rate=r2;
	}
	
	public void setHours(double h2)
	{
		hours=h2;
	}
	
	public String toString()
	{
		String out=super.toString()+" The rate is "+rate+" The hours is "+hours;
		return out;
	}
	
	public double benefitsCalculation(Jobs j6)
	{
		double benefits=0.00;
		if(j6==Jobs.ADMINISTRATIVE_SECRETARY)
		{
			benefits=(rate*hours)+(getYearsExperience()*15.00);
			return benefits;
		}
		else if(j6==Jobs.ADMINISTRATIVE_ASSISTANT)
		{
			benefits=(rate*hours)+(getYearsExperience()*25.00);
			return benefits;
		}
		else
		{
			return benefits;
		}
	}
}