package Client;
import java.util.*;
import java.io.*;
import Client.Services.*;
import Client.Services.Enums.*;
public class WorkerBenefits
{
	public static void main(String[] args)
	{
		WorkerBenefits wobe=new WorkerBenefits();
		wobe.displayData(wobe.listOfWorkers());
	}
	
	public ArrayList<Worker> listOfWorkers()
	{
		ArrayList<Worker> listOfW=new ArrayList();
		
		try
		{
			File f=new File("workers.txt");
			Scanner scan1=new Scanner(f);
			int index1=0;
			while(scan1.hasNextLine())
			{
				String line=scan1.nextLine();
				StringTokenizer stto=new StringTokenizer(line,",");
				String js=stto.nextToken();
				Jobs j15=Jobs.valueOf(js);
				String n15=stto.nextToken();
				String sss=stto.nextToken();
				int ss15=Integer.parseInt(sss);
				String yes=stto.nextToken();
				int ye15=Integer.parseInt(yes);
				
				if(j15.equals(Jobs.ELECTRICAL_ENGINEER))
				{
					String wbs=stto.nextToken();
					double wb15=Double.parseDouble(wbs);
					Engineer eW=new Engineer(n15,ss15,ye15,j15,wb15);
					listOfW.add(eW);
					
				}
				else if(j15.equals(Jobs.MECHANICAL_ENGINEER))
				{
					String wbs=stto.nextToken();
					double wb17=Double.parseDouble(wbs);
					Engineer eW=new Engineer(n15,ss15,ye15,j15,wb17);
					listOfW.add(eW);
					
				}
				else if(j15.equals(Jobs.ADMINISTRATIVE_ASSISTANT))
				{
					String rs=stto.nextToken();
					double r17=Double.parseDouble(rs);
					String hs=stto.nextToken();
					double h17=Double.parseDouble(hs);
					AdministrativePersonnel apW=new AdministrativePersonnel(n15,ss15,ye15,j15,r17,h17);
					listOfW.add(apW);
					
				}
				else if(j15.equals(Jobs.ADMINISTRATIVE_SECRETARY))
				{
					String rs=stto.nextToken();
					double r15=Double.parseDouble(rs);
					String hs=stto.nextToken();
					double h15=Double.parseDouble(hs);
					AdministrativePersonnel apW=new AdministrativePersonnel(n15,ss15,ye15,j15,r15,h15);
					listOfW.add(apW);
					
				}
				else if(j15.equals(Jobs.ENGINEERING_MANAGER))
				{
					String wbs=stto.nextToken();
					double wb20=Double.parseDouble(wbs);
					String bs=stto.nextToken();
					double b20=Double.parseDouble(bs);
					Management mW=new Management(n15,ss15,ye15,j15,wb20,b20);
					listOfW.add(mW);
					
				}
				else if(j15.equals(Jobs.ADMINISTRATIVE_MANAGER))
				{
					String wbs=stto.nextToken();
					double wb16=Double.parseDouble(wbs);
					String bs=stto.nextToken();
					double b15=Double.parseDouble(bs);
					Management mW=new Management(n15,ss15,ye15,j15,wb16,b15);
					listOfW.add(mW);
					
				}
				else{}
			}
		}
		catch(IOException ioe)
		{
		   System.out.println("Something is wrong");
		}
		return listOfW;
	}
	
	public void displayData(ArrayList<Worker> ale)
	{
		double bene=0.0;
		Engineer e=new Engineer();
		AdministrativePersonnel ap=new AdministrativePersonnel();
		Management m=new Management();
		Worker w;
		for(Worker out : ale)	
		{
			Jobs j30=out.getJobs();
			if(j30.equals(Jobs.ELECTRICAL_ENGINEER))
			{
				w=e;
				bene=out.benefitsCalculation(Jobs.ELECTRICAL_ENGINEER);
			}
			else if(j30.equals(Jobs.MECHANICAL_ENGINEER))
			{			
				w=e;
				bene=out.benefitsCalculation(Jobs.MECHANICAL_ENGINEER);
			}
			else if(j30.equals(Jobs.ADMINISTRATIVE_ASSISTANT))
			{			
				w=ap;
				bene=out.benefitsCalculation(Jobs.ADMINISTRATIVE_ASSISTANT);
			}
			else if(j30.equals(Jobs.ADMINISTRATIVE_SECRETARY))
			{
				w=ap;
				bene=out.benefitsCalculation(Jobs.ADMINISTRATIVE_SECRETARY);			
			}
			else if(j30.equals(Jobs.ENGINEERING_MANAGER))
			{
				w=m;
				bene=out.benefitsCalculation(Jobs.ENGINEERING_MANAGER);
			}
			else if(j30.equals(Jobs.ADMINISTRATIVE_MANAGER))
			{
				w=m;
				bene=out.benefitsCalculation(Jobs.ADMINISTRATIVE_MANAGER);			
			}
			else{}
			System.out.println("The benefits are "+bene+" "+out.toString());
			
			
		}	
	}
}