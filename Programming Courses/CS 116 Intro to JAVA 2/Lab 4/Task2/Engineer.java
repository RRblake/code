package Client.Services;
import Client.Services.Enums.*;
public class Engineer extends Worker
{
	private double weeklyBenefits;
	
	public Engineer()
	{
		super();
		weeklyBenefits=400.00;
	}
	
	public Engineer(String n4, int ss4, int ye4, Jobs j4, double wb)
	{
		super(n4, ss4, ye4, j4);
		weeklyBenefits=wb;
	}
	
	public double getWeeklyBenefits()
	{
		return weeklyBenefits;
	}
	
	public void setWeeklyBenefits(double wb2)
	{
		weeklyBenefits=wb2;
	}
	
	public String toString()
	{
		String out=super.toString()+" The weekly benefits are "+weeklyBenefits;
		return out;
	}
	
	public double benefitsCalculation(Jobs j5)
	{	
		double benefits=0.00;
		if(j5==Jobs.ELECTRICAL_ENGINEER)
		{
			benefits=weeklyBenefits+(getYearsExperience()*80.00);
			return benefits;
		}
		else if(j5==Jobs.MECHANICAL_ENGINEER)
		{
			benefits=(weeklyBenefits/2)+(getYearsExperience()*120.00);
			return benefits;
		}
		else
		{
			return benefits;
		}
	}
}