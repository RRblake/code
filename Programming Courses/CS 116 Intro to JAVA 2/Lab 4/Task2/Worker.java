package Client.Services;
import Client.Services.Enums.*;
public abstract class Worker
{
	public String name;
	public int socialSecurity;
	private int yearsExperience;
	public Jobs et;
	public static int id;
	public int currentID;
	
	public Worker()
	{
		name="Anyname";
		socialSecurity=12345;
		yearsExperience=0;
		et=Jobs.NONE;
		id++;
		currentID=id;
	}
	
	public Worker(String n, int ss, int ye, Jobs j)
	{
		name=n;
		socialSecurity=ss;
		yearsExperience=ye;
		et=j;
		id++;
		currentID=id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getSocialSecurity()
	{
		return socialSecurity;
	}
	
	public int getYearsExperience()
	{
		return yearsExperience;
	}
	
	public Jobs getJobs()
	{
		return et;
	}
	
	public void setName(String n2)
	{
		name=n2;
	}
	
	public void setSocialSecurity(int ss2)
	{
		socialSecurity=ss2;
	}
	
	public void getYearsExperience(int ye2)
	{
		yearsExperience=ye2;
	}
	
	public void setJobs(Jobs j2)
	{
		et=j2;
	}
	
	public String toString()
	{
		String out="The name is "+name+" The social security is "+socialSecurity+" The years of experience are "+yearsExperience+" The job is "+et+" The ID is "+currentID;
		return out;
	}
	public abstract double benefitsCalculation(Jobs j3);
}