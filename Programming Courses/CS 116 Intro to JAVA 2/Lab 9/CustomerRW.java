package folder1.folder2;
import java.util.*;
import java.io.*;
public class CustomerRW
{
	public void writeCustomer() 
	{
		Customer c1=new Customer("John1","Dow1",1);
		Customer c2=new Customer("John2","Dow2",2);
		Customer c3=new Customer("John3","Dow3",3);
		Customer c4=new Customer("John4","Dow4",4);
		Customer c5=new Customer("John5","Dow5",5);
		Customer c6=new Customer("John6","Dow6",6);
		Customer c7=new Customer("John7","Dow7",7);
		Customer c8=new Customer("John8","Dow8",8);
		Customer c9=new Customer("John9","Dow9",9);
		Customer c10=new Customer("John10","Dow10",10);
		Customer c11=new Customer("John11","Dow11",20);
		Customer c12=new Customer("John12","Dow12",21);
		Customer c13=new Customer("John13","Dow13",22);
		Customer c14=new Customer("John14","Dow14",23);
		Customer c15=new Customer("John15","Dow15",24);
		Customer c16=new Customer("John16","Dow16",36);
		Customer c17=new Customer("John17","Dow17",37);
		Customer c18=new Customer("John18","Dow18",38);
		Customer c19=new Customer("John19","Dow19",39);
		Customer c20=new Customer("John20","Dow20",40);
		Customer c21=new Customer("John21","Dow21",41);
		Customer c22=new Customer("John22","Dow22",42);
		Customer c23=new Customer("John23","Dow23",43);
		Customer c24=new Customer("John24","Dow24",44);
		Customer c25=new Customer("John25","Dow25",45);
	
		Customer[] cArray=new Customer[25];
		cArray[0]=c1;
		cArray[1]=c2;
		cArray[2]=c3;
		cArray[3]=c4;
		cArray[4]=c5;
		cArray[5]=c6;
		cArray[6]=c7;
		cArray[7]=c8;
		cArray[8]=c9;
		cArray[9]=c10;
		cArray[10]=c11;
		cArray[11]=c12;
		cArray[12]=c13;
		cArray[13]=c14;
		cArray[14]=c15;
		cArray[15]=c16;
		cArray[16]=c17;
		cArray[17]=c18;
		cArray[18]=c19;
		cArray[19]=c20;
		cArray[20]=c21;
		cArray[21]=c22;
		cArray[22]=c23;
		cArray[23]=c24;
		cArray[24]=c25;
		
		try
		{
			FileOutputStream fosC=new FileOutputStream("Customers.ser");
			ObjectOutputStream oosC=new ObjectOutputStream(fosC);
			for(int i=0;i<=cArray.length;i++)
			{
				oosC.writeObject(cArray[i]);
			}			
		}
		catch(IOException ioe)
		{
			System.out.println("Error writing into Customers.ser.");
			ioe.printStackTrace();
		}
	}
	
	public ArrayList<Customer> readCustomer()
	{
		ArrayList<Customer> list=new ArrayList<Customer>(25);
		Customer readC=null;
		try
		{
			FileInputStream fisC=new FileInputStream("Customers.ser");
			ObjectInputStream oisC=new ObjectInputStream(fisC);
			while(true)
			{
				try
				{
					readC=(Customer)oisC.readObject();
					list.add(readC);
				}
				catch(EOFException eof)
				{
					System.out.println(eof.getMessage());
				}
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();				 
		}
		catch(ClassNotFoundException cnf)
		{
			cnf.printStackTrace();
		}
		return list;
	}
	
}