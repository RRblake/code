package folder1.folder2;
import java.util.*;
import java.io.*;
public class Customer implements Serializable
{
	String firstName;
	String lastName;
	int lottoTicket;
	CustomerTypes type;
	public Customer()
	{
		firstName=" ";
		lastName=" ";
		lottoTicket=0;
		type=null;
	}
	
	public Customer(String fn,String ln,int lt)
	{
		firstName=fn;
		lastName=ln;
		lottoTicket=lt;
		type=null;
	}
	
	public int getLottoTicket()
	{
		return lottoTicket;
	}
	
	public void setCustomerType(CustomerTypes t)
	{
		type=t;
	}
}