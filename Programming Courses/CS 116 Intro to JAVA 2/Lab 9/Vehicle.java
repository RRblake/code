package folder1.folder2;
import java.io.*;
import java.util.*;
import folder1.folder2.*;
public class Vehicle implements Serializable
{
	VehicleTypes type;
	double acceleration;
	double velocity;
	double distance;
	double time;
	double price;
	static int id=1;
	int vehicleID;
	
	public Vehicle()
	{
		acceleration=0.0;
		velocity=0.0;
		type=null;
		distance=0.0;
		time=0.0;
		price=0.0;
		vehicleID=id;
		id++;
	}
	
	public Vehicle(VehicleTypes vt,double a,double t,double v,double d)
	{
		acceleration=a;
		velocity=v;
		type=vt;
		distance=d;
		time=t;
		price=0.0;
		vehicleID=id;
		id++;
	}
	
	public int getVehicleID()
	{
		return vehicleID;
	}
	
	public void setPrice(double p)
	{
		price=p;
	}
	
	public boolean equals(Vehicle v)
	{
		if(this.getVehicleID()==v.getVehicleID())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}