package folder1.folder2;
import java.util.*;
import java.io.*;
public class DealershipRW
{
	public void writeDealership() 
	{
		Dealership d1=new Dealership("Glenn Ellyn Chevrolet",DealershipTypes.CHEVEROLET);
		Dealership d2=new Dealership("South Suburbs Pontiac",DealershipTypes.PONTIAC);
		Dealership d3=new Dealership("Chicago Buick",DealershipTypes.BUICK);
		Dealership d4=new Dealership("Schaumburg Cadillac",DealershipTypes.CADILLAC);
		Dealership d5=new Dealership("Saturn of North Chicago",DealershipTypes.SATURN);
	
		Dealership[] dArray=new Dealership[5];
		dArray[0]=d1;
		dArray[1]=d2;
		dArray[2]=d3;
		dArray[3]=d4;
		dArray[4]=d5;
		
		try
		{
			FileOutputStream fosD=new FileOutputStream("Dealerships.ser");
			ObjectOutputStream oosD=new ObjectOutputStream(fosD);
			for(int i=0;i<=dArray.length;i++)
			{
				oosD.writeObject(dArray[i]);
			}			
		}
		catch(IOException ioe)
		{
			System.out.println("Error writing into Dealerships.ser.");
			ioe.printStackTrace();
		}
	}
	
	public ArrayList<Dealership> readDealership()
	{
		ArrayList<Dealership> list=new ArrayList<Dealership>(5);
		Dealership readD=null;
		try
		{
			FileInputStream fisD=new FileInputStream("Dealerships.ser");
			ObjectInputStream oisD=new ObjectInputStream(fisD);
			while(true)
			{
				try
				{
					readD=(Dealership)oisD.readObject();
					list.add(readD);
				}
				catch(EOFException eof)
				{
					System.out.println(eof.getMessage());
				}
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();				 
		}
		catch(ClassNotFoundException cnf)
		{
			cnf.printStackTrace();
		}
		return list;
	} 
	
}