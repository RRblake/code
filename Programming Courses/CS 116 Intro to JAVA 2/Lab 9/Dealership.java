package folder1.folder2;
import java.io.*;
import java.util.*;
public class Dealership implements Serializable
{
	String name;
	DealershipTypes type;
	
	public Dealership()
	{
		name=" ";
		type=null;
	}
	
	public Dealership(String n, DealershipTypes t)
	{
		name=n;
		type=t;
	}
	
	public DealershipTypes getDealershipTypes()
	{
		return type;
	}
	
	public void setDealershipTypes(DealershipTypes t1)
	{
		type=t1;
	}
	
}