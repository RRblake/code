package folder1.folder2;
import java.util.*;
import java.io.*;
public class VehicleRW
{
	public void writeVehicle() 
	{
		Vehicle v1=new Vehicle(VehicleTypes.FOUR_DOOR_SEDAN,1,1,0,0);
		Vehicle v2=new Vehicle(VehicleTypes.FOUR_DOOR_SEDAN,2,1,0,0);
		Vehicle v3=new Vehicle(VehicleTypes.TWO_DOOR_COUPE,3,1,0,0);
		Vehicle v4=new Vehicle(VehicleTypes.TWO_DOOR_COUPE,4,1,0,0);
		Vehicle v5=new Vehicle(VehicleTypes.TRUCK,5,1,0,0);
		Vehicle v6=new Vehicle(VehicleTypes.TRUCK,6,1,0,0);
		Vehicle v7=new Vehicle(VehicleTypes.SUV,7,1,0,0);
		Vehicle v8=new Vehicle(VehicleTypes.SPORT,8,1,0,0);
		Vehicle v9=new Vehicle(VehicleTypes.SPORT,9,1,0,0);
		Vehicle v10=new Vehicle(VehicleTypes.MINI_VAN,10,1,0,0);
	
		Vehicle[] vArray=new Vehicle[10];
		vArray[0]=v1;
		vArray[1]=v2;
		vArray[2]=v3;
		vArray[3]=v4;
		vArray[4]=v5;
		vArray[5]=v6;
		vArray[6]=v7;
		vArray[7]=v8;
		vArray[8]=v9;
		vArray[9]=v10;
		try
		{
			FileOutputStream fosV=new FileOutputStream("Vehicles.ser");
			ObjectOutputStream oosV=new ObjectOutputStream(fosV);
			for(int i=0;i<=vArray.length;i++)
			{
				oosV.writeObject(vArray[i]);
			}			
		}
		catch(IOException ioe)
		{
			System.out.println("Error writing into Vehicles.ser.");
			ioe.printStackTrace();
		}
	}
	
	public ArrayList<Vehicle> readVehicle()
	{
		ArrayList<Vehicle> list=new ArrayList<Vehicle>(10);
		Vehicle readV=null;
		try
		{
			FileInputStream fisV=new FileInputStream("Vehicles.ser");
			ObjectInputStream oisV=new ObjectInputStream(fisV);
			while(true)
			{
				try
				{
					readV=(Vehicle)oisV.readObject();
					list.add(readV);
				}
				catch(EOFException eof)
				{
					System.out.println(eof.getMessage());
				}
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();				 
		}
		catch(ClassNotFoundException cnf)
		{
			cnf.printStackTrace();
		}
		return list;
	}  
	
}