package randy.remblake;
import java.util.*;
import java.text.*;

public class Rod
{
	MaterialCategories mc=null;
	String name="";
	double area=0.0;
	double length=0.0;
	static int id=0;
	int rodID=0;
	
	public Rod()
	{		
		id++;
		rodID=id;
		mc=null;
		name="any";
		area=-0.1;
		length=-0.1;
	}
	
	public Rod(String na, double ar, double le)
	{		
		id++;
		rodID=id;
		mc=null;
		name=na;
		area=ar;
		length=le;
	}
	
	public int getID()
	{
		return id;
	}
	
	public int getRodID()
	{
		return rodID;
	}
	
	public MaterialCategories getMat()
	{
		return mc;
	}

	public String getName()
	{
		return name;
	}

	public double getArea()
	{
		return area;
	}

	public double getLength()
	{
		return length;
	}
	
	public void setID(int i)
	{
		id=i;
	}
	
	public void setRodID(int rid)
	{
		rodID=rid;
	}
	
	public void setMat(MaterialCategories mat)
	{
		mc=mat;
	}

	public void setName(String na)
	{
		name=na;
	}

	public void setArea(double ar)
	{
		area=ar;
	}

	public void setLength(double le)
	{
		length=le;
	}
	
	public String toString()
	{
		String output="The category is: "+mc+" "+"The name of the material is "+name+" "+"The length is "+length+" "+"The cross area is "+
		+area+"The object id is "+rodID;
		return output;
	}
	
	public double calculateExpansion(double tempchan, double theexp)
	{
		double DL=theexp*length*tempchan;
				
		DecimalFormat myFormatter1 = new DecimalFormat("###.####");
		String stroutput1 = myFormatter1.format(DL);
		double output1=Double.parseDouble(stroutput1);
		return output1;
	}
	
	public double calculateForce(double youngs, double lencha)
	{
		double force=youngs*area*lencha/length;
		
		DecimalFormat myFormatter2 = new DecimalFormat("###.##");
		String stroutput2 = myFormatter2.format(force);
		double output2=Double.valueOf(stroutput2);
		return output2;
	}
}