package randy;
import randy.remblake.Rod;
import randy.remblake.MaterialCategories;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

public class RodClient
{
	public static void main(String [] args)
	{
		Rod[] rdar=null;
		double[] dtar=null;
		double[] yar=null;
		try
		{
			File f=new File("coefficients.txt");
			Scanner scan1=new Scanner(f);
			int index1=0;
			int index2=0;
			while(scan1.hasNextLine())
			{
				String line1=scan1.nextLine();
				index1++;
			}
			rdar=new Rod[index1];
			dtar=new double[index1];
			yar=new double[index1];
			Scanner scan2=new Scanner(f);
			while(scan2.hasNextLine())
			{
				String line2=scan2.nextLine();
				StringTokenizer stt=new StringTokenizer(line2,",");
				String strmats=stt.nextToken();
				MaterialCategories mats=MaterialCategories.valueOf(strmats);
				String nam=stt.nextToken();
				String ares=stt.nextToken();
				double are=Double.parseDouble(ares);
				String lens=stt.nextToken();
				double len=Double.parseDouble(lens);
				String dtems=stt.nextToken();
				double dtem=Double.parseDouble(dtems);
				String yons=stt.nextToken();
				double yon=Double.parseDouble(yons);
				Rod r=new Rod();
				rdar[index2]=r;
				rdar[index2].setName(nam);
				rdar[index2].setArea(are);
				rdar[index2].setLength(len);
				rdar[index2].setMat(mats);
				dtar[index2]=dtem;
				yar[index2]=yon;
				index2++;
			}
		}
		
		catch (IOException ioe)
		{
			System.out.println("Something went wrong.");
		}
				
		/*String arg1=args[0];
		String exsists="textexsists";
		String not="notready";
		*/
		//if (arg1==exsists)
		//{
			Scanner scan3=new Scanner(System.in);
			double[] foar=new double[rdar.length];
		
			for(int i=0; i<=rdar.length-1;i++)
			{
				System.out.println("Please enter the change in temperature for material "+rdar[i].getName());
				double dite=scan3.nextDouble();
				double caldl=rdar[i].calculateExpansion(dite, dtar[i]);
				foar[i]=rdar[i].calculateForce(yar[i], caldl);
			
				System.out.println("_______FIRST OUTPUT REQUIRED______");
				System.out.println("The expansion is: "+caldl);
				System.out.println("The Force is: "+foar[i]);
			
			}
		
			int max=0;
			for(int i=0; i<=foar.length-1;i++)
			{
				if(foar[i]>foar[max])
				{
					max=i;
				}
			}
			System.out.println("_______SECOND OUTPUT REQUIRED______");
			System.out.println("The greatest force required is: "+foar[max]);
			System.out.println(rdar[max].toString());
			System.out.println("_______THIRD OUTPUT REQUIRED______");
			for(int i=0; i<=rdar.length-1;i++)
			{
				System.out.println(rdar[i].toString());
			}
		//}
		/*else if(arg1==not)
		{
			System.out.println("I can't read data unless text file is ready.");
		} 		
		*/
	}
}