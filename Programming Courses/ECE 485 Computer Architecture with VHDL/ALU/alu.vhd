library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;



---------------------------------------------------

entity ALU is

port(	Clk:	in std_logic;
	A:	in std_logic_vector(7 downto 0);
	B:	in std_logic_vector(7 downto 0);
	Sel:	in std_logic_vector(2 downto 0);
	F:	out std_logic_vector(7 downto 0)  
);

end ALU;

---------------------------------------------------

architecture behv of ALU is
begin					   
    
    
    process(Clk)
    begin
    
	-- use case statement to achieve 
	-- different operations of ALU
        if(rising_edge(Clk)) then
		case Sel is
		    when "000" =>
			F <= A and B;
		    when "001" =>						
		        F <= not A;
		    when "010" =>
			F <= A + B;
		    when "011" =>  
 			if (A + (not B) -1 > 0) then
				F <= "00000001";
			else
				F <= "00000000";
			end if;           
	 	    when "100" =>	 
			F <= A + (not B) - 1;	
		    when "101" =>	 
			F <= A xor B;	
	            when "110" =>	 
			F <= to_stdlogicvector(to_bitvector(A) sll 1);
	            when "111" =>	 
			F <= to_stdlogicvector(to_bitvector(A) sra 1);
		    when others =>	 
			F <= "XXXXXXXX";
        	end case;
	end if;
    end process;

end behv;

