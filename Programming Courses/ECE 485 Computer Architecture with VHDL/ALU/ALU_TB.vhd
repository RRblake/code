library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---------------------------------------------
entity ALU_TB is
end ALU_TB;
---------------------------------------------  
architecture behavior of ALU_TB is
  
     
    component alu
    port(
         Clk : in  std_logic;
         A : in  std_logic_vector(7 downto 0);
         B : in  std_logic_vector(7 downto 0);
         Sel : in  std_logic_vector(2 downto 0);
         F : out  std_logic_vector(7 downto 0)
        );
    end component;
     
 
   --Inputs
   signal Clk : std_logic := '0';
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal B : std_logic_vector(7 downto 0) := (others => '0');
   signal Sel : std_logic_vector(2 downto 0) := (others => '0');
 
    --Outputs
   signal F : std_logic_vector(7 downto 0);
 
   -- Clock period definitions
   constant Clk_period : time := 20 ns;
  
begin
  
    -- Instantiate the Unit Under Test (UUT)
   uut: alu port map (
          Clk => Clk,
          A => A,
          B => B,
          Sel => Sel,
          F => F
        );
 
   -- Clock process definitions
   Clk_process :process
   begin
        Clk <= '0';
        wait for Clk_period/2;
        Clk <= '1';
        wait for Clk_period/2;
   end process;
  
 
   -- Stimulus process
   stim_proc: process
   begin       
      -- hold reset state for 100 ns.
      wait for 100 ns;  
            A <= "00111001";
            B <= "01001111";
             
            Sel <= "000";
            wait for 20 ns;
            Sel <= "001";
            wait for 20 ns;
            Sel <= "010";
            wait for 20 ns;
            Sel <= "011";
            wait for 20 ns;
            Sel <= "100";
            wait for 20 ns;
            Sel <= "101";
            wait for 20 ns;
            Sel <= "110";
            wait for 20 ns;
            Sel <= "111";
      wait;
   end process;
 ---------------------------------------------
END;
