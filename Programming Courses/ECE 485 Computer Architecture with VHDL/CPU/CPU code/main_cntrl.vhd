library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity main_cntrl is
port(
	op : in std_logic_vector(5 downto 0);
	alu_src : out std_logic;
	mem_rewr : out std_logic;
	out_to_reg : out std_logic;
	writeEn : out std_logic;
	beq_sel : out std_logic;
	write_reg_sel : out std_logic;
	alu_op : out std_logic_vector(3 downto 0)
);
end main_cntrl;

architecture behv of main_cntrl is
signal alu_src_inv : std_logic;
signal writeEn_inv : std_logic;
signal write_reg_sel_inv : std_logic;

begin
	

	
	
		alu_src_inv <= ((not(op(5)))and(not(op(4)))and(not(op(3)))and(not(op(2)))and(not(op(1)))and(not(op(0)))) or ((not(op(5)))and(not(op(4)))and(not(op(3)))and(op(2))and(not(op(1)))and(not(op(0)))); 
		alu_src <= not(alu_src_inv);

		mem_rewr <= (op(5))and(not(op(4)))and(op(3))and(not(op(2)))and(op(1))and(op(0));

		out_to_reg <= (op(5))and(not(op(4)))and(not(op(3)))and(not(op(2)))and(op(1))and(op(0));
		
		writeEn_inv <= ((op(5))and(not(op(4)))and(op(3))and(not(op(2)))and(op(1))and(op(0))) or ((not(op(5)))and(not(op(4)))and(not(op(3)))and(op(2))and(not(op(1)))and(not(op(0))));
		writeEn <= not(writeEn_inv);

		beq_sel <= (not(op(5)))and(not(op(4)))and(not(op(3)))and(op(2))and(not(op(1)))and(not(op(0)));

		write_reg_sel_inv <= (not(op(5)))and(not(op(4)))and(not(op(3)))and(not(op(2)))and(not(op(1)))and(not(op(0)));
		write_reg_sel <= not(write_reg_sel_inv);

		alu_op(3) <= (not(op(5)))and(not(op(4)))and(not(op(3)))and(not(op(2)))and(not(op(1)))and(not(op(0)));
		alu_op(2) <= (not(op(5)))and(not(op(4)))and(op(3))and(op(2))and(not(op(1)))and(op(0));
		alu_op(1) <= ((not(op(5)))and(not(op(4)))and(op(3))and(not(op(2)))and(op(1))and(op(0))) or ((not(op(5)))and(not(op(4)))and(op(3))and(not(op(2)))and(op(1))and(not(op(0)))) or ((not(op(5)))and(not(op(4)))and(not(op(3)))and(op(2))and(not(op(1)))and(not(op(0))));
		alu_op(0) <= ((not(op(5)))and(not(op(4)))and(op(3))and(op(2))and(not(op(1)))and(not(op(0)))) or ((not(op(5)))and(not(op(4)))and(op(3))and(op(2))and(not(op(1)))and(op(0))) or ((not(op(5)))and(not(op(4)))and(not(op(3)))and(op(2))and(not(op(1)))and(not(op(0)))) or((not(op(5)))and(not(op(4)))and(op(3))and(not(op(2))and(op(1))and(op(0))));
	
end behv;