library	ieee;
use ieee.std_logic_1164.all;  
 
use ieee.numeric_std.all; 


entity pc is
port ( 	
	clock : in std_logic; 	  
	inPC : in std_logic_vector(31 downto 0);
	outPC : out std_logic_vector(31 downto 0)
);
end pc;

architecture behv of pc is			


  signal counter : std_logic_vector(31 downto 0) := (others => '0');

begin

  write: process(clock, inPC)
  begin
	if (clock'event and clock = '1') then
	    counter <= inPC;
	  
	end if; 
  end process;						   
	
  read: process(clock)
  begin
	outPC <= counter;
  end process;
		
end behv;
