library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---------------------------------------------
entity cpu_TB is
end cpu_TB;
---------------------------------------------  
architecture behavior of cpu_TB is
  
     
    component cpu
    port(
	 clk	: 	in std_logic; 	
	 cpu_out : out std_logic_vector(31 downto 0)

    );
    end component;
     
 
   --Inputs
   signal clk : std_logic := '0';
 
    --Outputs
   signal cpu_out : std_logic_vector(31 downto 0) := (others => '0');

   -- Clock period definitions
   constant Clk_period : time := 50 ns;
  
begin
  
    -- Instantiate the Unit Under Test (UUT)
   uut: cpu port map (
          clk => clk,
	  cpu_out => cpu_out
        );
 
   -- Clock process definitions
      Clk_process :process
   begin
        clk <= '0';
        wait for Clk_period/2;
        clk <= '1';
        wait for Clk_period/2;
   end process;
  
 
   -- Stimulus process
   stim_proc: process
   begin        	
   wait;
   end process;
 ---------------------------------------------
END;
