library	ieee;
use ieee.std_logic_1164.all;  
 
use ieee.numeric_std.all; 


entity reg_file is
port ( 	
	clock	: 	in std_logic; 	
	writeEn	: 	in std_logic;	
	writeRegsel	: 	in std_logic_vector(3 downto 0);  
	regAsel	: 	in std_logic_vector(3 downto 0);
	regBsel	: 	in std_logic_vector(3 downto 0);
	writeIn	: 	in std_logic_vector(31 downto 0);
	outA	: 	out std_logic_vector(31 downto 0);
	outB	:	out std_logic_vector(31 downto 0)
);
end reg_file;

architecture behv of reg_file is			

  type rf_type is array (0 to 15) of 
        std_logic_vector(31 downto 0);
  signal registers: rf_type := (others => "00000000000000000000000000000000");

begin

  write: process(clock, writeIn, writeEn, writeRegsel)
  begin
	if (clock'event and clock = '1') then
	  if writeEn='1' then
	    registers(to_integer(unsigned(writeRegsel))) <= writeIn;
	  end if;
	end if; 
  end process;						   
	
  read: process(clock, regAsel, regBsel)
  begin

							 
	    outA <= registers(to_integer(unsigned(regAsel)));
	    outB <= registers(to_integer(unsigned(regBsel)));

  end process;
	

	
end behv;


