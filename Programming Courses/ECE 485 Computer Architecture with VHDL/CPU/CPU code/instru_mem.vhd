library	ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;   

entity instru_mem is
port ( 		
		address	: in std_logic_vector(31 downto 0);
		data_out : out std_logic_vector(31 downto 0)
);
end instru_mem;

architecture behv of instru_mem is			

  type ram_type is array (0 to 255) of 
        		std_logic_vector(31 downto 0);
  signal ram: ram_type := (	"00000000000000000000000000000000", --nop
				"00001000000100010000000000001111", --addi $1 $1 0xF
				"00001101001000101011111011101111", --ori $2 $2 0xBEEF
				"00000000001100010011100000000000", --add $3 $3 $1
				"00101011001100010000000000001010", --sw $1 10($3)
				"00101011001100100000000000010100", --sw $2 20($3)
				"00101011001100110000000000011110", --sw $3 30($3)
				"00000100001100010000000000000010", --beq $3 $1 0x2
				"00100011001101000000000000011110", --lw $4 30($3)
				"00000000001001000001100011000000", --sub $1 $2 $4
				"00000000001000010101100010000000", --nand $5 $2 $1
				"00001001010101100000110010101011", --andi $6 $5 0xCAB
				"00000000011000010111100101000000", --or $7 $6 $1
				others => "00000000000000000000000000000000");

begin
    read: process(address)
	begin				 
		data_out <= ram(to_integer(unsigned(address)));
	end process;
end behv;
