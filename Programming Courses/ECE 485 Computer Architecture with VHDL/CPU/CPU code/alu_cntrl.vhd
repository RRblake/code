library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity alu_cntrl is
port(
	op_alu : in std_logic_vector(3 downto 0);
	func : in std_logic_vector(5 downto 0);
	alu_s : out std_logic_vector(2 downto 0)
);
end alu_cntrl;

architecture behv of alu_cntrl is
begin
	process(op_alu, func)
	begin
		alu_s(2) <= ((not(op_alu(3)))and(not(op_alu(2)))and(op_alu(1))and(not(op_alu(0))))or((op_alu(3))and(not(func(2)))and(func(1))and(not(func(0))));
		
		alu_s(1) <= ((not(op_alu(3)))and(not(op_alu(1)))and(op_alu(0)))or((op_alu(3))and(not(func(1)))and(func(0)));

		alu_s(0) <= ((not(op_alu(3)))and(not(op_alu(2)))and(op_alu(1))and(op_alu(0)))or((not(op_alu(3)))and(op_alu(2))and(not(op_alu(1)))and(op_alu(0)))or((op_alu(3))and(not(func(2)))and(func(1))and(func(0)))or((op_alu(3))and(func(2))and(not(func(1)))and(func(0)));
	end process;
end behv;
