library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------

entity Mux_1 is
port(	
	mux_A_1: 	in std_logic;
	mux_B_1: 	in std_logic;
	mux_S_1:	in std_logic;
	mux_F_1:	out std_logic
);
end Mux_1;  

-------------------------------------------------

architecture behv of Mux_1 is
begin
    process(mux_A_1, mux_B_1, mux_S_1)
    begin
    
        -- use case statement
        case mux_S_1 is
	    when '0' => mux_F_1 <= mux_A_1;
	    when '1' => mux_F_1 <= mux_B_1;
    	    when others => mux_F_1 <= 'X';
	end case;

    end process;
end behv;
