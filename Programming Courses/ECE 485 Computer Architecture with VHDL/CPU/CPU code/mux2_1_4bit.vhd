library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------

entity Mux_4 is
port(	
	mux_A_4: 	in std_logic_vector(3 downto 0);
	mux_B_4: 	in std_logic_vector(3 downto 0);
	mux_S_4:	in std_logic;
	mux_F_4:	out std_logic_vector(3 downto 0)
);
end Mux_4;  

-------------------------------------------------

architecture behv of Mux_4 is
begin
    process(mux_A_4, mux_B_4, mux_S_4)
    begin
    
        -- use case statement
        case mux_S_4 is
	    when '0' => mux_F_4 <= mux_A_4;
	    when '1' => mux_F_4 <= mux_B_4;
    	    when others => mux_F_4 <= "XXXX";
	end case;

    end process;
end behv;
