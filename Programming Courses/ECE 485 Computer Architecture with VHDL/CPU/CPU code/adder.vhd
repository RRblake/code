library	ieee;
use ieee.std_logic_1164.all;   
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity adder is 
port(
	addA : in std_logic_vector(31 downto 0);
	addB : in std_logic_vector(31 downto 0);
	addF : out std_logic_vector(31 downto 0)
);
end adder;

architecture behav of adder is
begin
	addF <= addA + addB;
end behav;