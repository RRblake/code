
/*
 * Randy Remblake
 * CS201 - Sec1
 * 130825
 * 9/6/13
 * 
 *  This program prints out your birthdate and sets up a friends birthdate then prints out that as well.
 */





public class RemblakeRBirthDate{
	
	public static int Month;
	public static int Day;
	public static int Year;
	
	//Default constructor
	public RemblakeRBirthDate() {
		Month = 7;
		Day = 26;
		Year = 1992;
		}
	
	//Nondefault constructor
	public RemblakeRBirthDate(int Mdate, int Ddate, int Ydate) {
		Month = Mdate;
		Day = Ddate;
		Year = Ydate;
		
		
	}
	
	//Accessors for variables
	public static int getMonth() {
		return Month;
	}
	
	public static int getDay() {
		return Day;
	}
	
	public static int getYear() {
		return Year;
	}
	
	//Mutators for variables
	public static void setMonth(int month) {
		Month = month;
	}

	public static void setDay(int day) {
		Day = day;
	}

	public static void setYear(int year) {
		Year = year;
	}
	
	//Method to print out variables
	public String toString() {
		return Month + "/" + Day + "/" + Year;
	}
	
	
	
	// Sets up restrictions for the Year variable
	public static boolean validY() {
		if ((getYear() >= 1900) && (getYear() <= 3000))
			return true;
		else return false;
	}
	
	//Sets up restrictions for the Month variable
	public static boolean validM() {
		if ((getMonth() >= 1) && (getMonth() <= 12))
			return true;
		else return false;
	}

	
	public static void main(String[] arg) {
		
		//Creates object myBirthDate 
		RemblakeRBirthDate myBirthDate = new RemblakeRBirthDate();
		
		//Mutates the data of myBirthDate
		setMonth(7);
		setDay(26);
		setYear(1992);
		
		//Prints out results depending on if the case if true or false
		if ((validY() == true) && (validM() == true)) {
			System.out.println("Valid dates!");
			System.out.println(RemblakeRBirthDate.getMonth());
			System.out.println(RemblakeRBirthDate.getDay());
			System.out.println(RemblakeRBirthDate.getYear());
		}
		else System.out.println("Invalid dates!");
		
		
		//Creates object friendBirthDate
		RemblakeRBirthDate friendBirthDate = new RemblakeRBirthDate(10, 25, 1989);
		
		//Prints out friendBirthDate
		System.out.println(friendBirthDate.toString());

	}


	
	
	

}
