
public class Exercise1 {
	
	public static int num3(int num1, int num2) {
		
		return num1 + num2;
	}
	
	public static void main(String[] args) {
		
		System.out.println(num3(5,10));
		int num4 = num3(10,10);
		System.out.println(num3(num4,5));
	
	}

}
