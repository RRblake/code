---------------------------------
Bridge
---------------------------------
Test #1

Test Case		Commands	Expected Result	Checked

Insert at end		+a +b +c +d	a b c D		yes
Travel from start	< N N 		a b C d		yes
Travel from end		> P P		a B c d		yes
Delete middle		-		a C d		yes
Insert at middle	+e +f +f	a c e f F d	yes
Remove last		> -		A c e f f	yes
Remove first		-		C e f f		yes
Display element		@		Returns c	yes
Replace element		=g		G e f f		yes
Clear the list		C		Empty List	yes

Test #2

Test Case		Commands	Expected Result	Checked

Insert at end		+1 +2 +3 +4	1 2 3 4!	yes
Travel from start	< N N 		1 2 3! 4	yes
Travel from end		> P P		1 2! 3 4	yes
Delete middle		-		1 3! 4		yes
Insert at middle	+5 +6 +6	1 3 5 6 6! 4	yes
Remove last		> -		1! 3 5 6 6	yes
Remove first		-		3! 5 6 6	yes
Display element		@		Returns 3	yes
Replace element		=7		7! 5 6 6	yes
Clear the list		C		Empty List	yes
---------------------------------
In-Lab #1
---------------------------------
Test

Test case	Commands	Expected Result	Checked

Set up List	+a +b +c +d	a b c d		yes
Move last	M		D a b c		yes
Move second	N M		A d b c		yes
Move third	N N M		B a d c		yes																
---------------------------------
Postlab #1
---------------------------------
Insert
O(1)
There is no looping in the method as it only goes through each line once.

Remove
O(N)
In order to remove a node we need to set the one before it and link it with the one after it.
In order to go backwards though you need to actually go forward in the list until you hit
the one right before the node you are trying to get rid of. This of course requires looping.

gotoNext()
O(1)
Same reasoning behind the Insert method.

gotoPrev()
O(N)
Same reasoning behind the Remove method.


