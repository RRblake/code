//--------------------------------------------------------------------
//
//  Laboratory 5                                       StackNode.jshl
//
//  Class definition for a linked list implementation
//  
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

//Facilitator class for the Stack class
class StackNode                     // A singly linked list node
{  
  // Data members
  private Object element;           // Object for this node
  private StackNode next;           // Reference to next node in list
  
  // because there are no access labels (public, private or protected),
  // access is limited to the package where these methods are declared
  public StackNode(Object newElement, StackNode nextval) // Constructor 
  { element = newElement;
	next = nextval;
	}  

  // Class methods
  // Other classes in this package need to know about next and element
  // or set next
  public StackNode getNext( )                  // Returns reference to next node
  { return next;                    }     
  public Object getElement( )                  // Returns element's value
  { return element;                 }
  public void setNext( StackNode nextVal )     // Sets value of next
  { this.next = nextVal;                 }
  
} // class StackNode
