---------------------------------
Bridge
---------------------------------
Test1

Test case		Commands	Expected Result	Checked

Series of pushes	+a +b +c +d	a b c D		yes
Series of pops		- - -		A		yes
More pushes		+e +f		a e F		yes
More pops		- -		A		yes
Empty? Full?		E F		false false	yes
Empty the stack		-		Empty Stack	yes
Empty? Full?		E F		true false	yes
																
---------------------------------
Postlab #1
---------------------------------
Part A

"abc"
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());
-------------------------------------
"acb"
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

System.out.print(permuteStack.pop());
System.out.print(permuteStack.pop());
-------------------------------------
"bac"
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

System.out.print(permuteStack.pop());
System.out.print(permuteStack.pop());

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());
-------------------------------------
"bca"
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));
System.out.print(permuteStack.pop());

System.out.print(permuteStack.pop());
-------------------------------------
"cab"
We can't code this one because we need to display a after c but
b is on top of a in the stack.
-------------------------------------
"cba"
Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

Ch = (char) System.in.read();
permuteStack.push(new Character(ch));

System.out.print(permuteStack.pop());
System.out.print(permuteStack.pop());
System.out.print(permuteStack.pop());
-------------------------------------
Part B
dcba
This is the only one that can be produced because it is the last one to get put on the stack.
so everything that is before it can only be popped out of the stack because they have been read
and pushed onto the stack.