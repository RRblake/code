//--------------------------------------------------------------------
//
//  Laboratory 5                                        AStack.jshl
//
//  Definitions for the array implementation of the Stack ADT
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

class AStackDown implements Stack    // Array based stack class
{
  // Data members
  private int top;               // Index for the top element
  private Object [] element;     // Array containing stack elements

  // Constructors
  public AStackDown( )                     // Constructor: default size
  { 	setup (DEF_MAX_STACK_SIZE);
		top = element.length - 1; }
  public AStackDown(int size)              // Constructor: specific size
  {     setup (size);
		top = element.length - 1; }
  
  // Class methods
  private void setup(int size)         // Called by Constructors only
  {      element = new Object[size];  }

  // --- Complete these method implementations for the interface Stack  --- //
  
  // Stack manipulation operations
  public void push(Object newElement)     // Push Object onto stack
  {                         
    // make sure newElement isn't null and stack isn't full
    if (newElement == null)
    {
        System.out.println("Element is null -- added nothing to the stack");
        return;
    }
    if (isFull( ))
    {
        System.out.println("Stack is full -- added nothing to the stack");
        return;
    }
    // add code here to add an element to the top of the stack
	element[top] = newElement;
	top--;  
  }
    
  public Object pop( )                // Pop Object from top of stack
  { 
    // make sure stack isn't empty
    if(isEmpty( ))
    {
        System.out.println("Empty stack -- removed nothing from stack");
        return null;
    }
    // add code here to remove an element from the top of the stack and
    // return its value
    Object o = element[top + 1];
	element[top + 1] = null;
	top++; 
	return o;
  }
  
  public void clear()
  {
	for (int i = 0; i<top; i++)
	{
		element[i] = null;
		top = 0;
	}
  }
  
  // --- Define remaining methods for the interface Stack here --- //
  
  public boolean isEmpty()
  {
	if(top == element.length - 1)
	{
		return true;
	}
	else
	{
		return false;
	}
  }
  
  public boolean isFull()
  {
	if(top == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
  }
  public void showStructure ( )
  // Array implementation. Outputs the elements in a stack. If the
  // stack is empty, outputs "Empty stack". This operation is intended
  // for testing and debugging purposes only.
  {
    int j;   // Loop counter

    if (isEmpty( ))
       System.out.println("Stack is empty");
    else
    {
       System.out.print("top = ");
       System.out.println(top);
       for ( j = 0 ; j < element.length ; j++ )
           System.out.print(j + "\t");
       System.out.println( );
       for ( j = top ; j > 0  ; j-- )
       {   
           System.out.print(element[j]);
       System.out.print("\t");
       }    
       System.out.println( );
     }
   } // showStructure for AStack
      
} // class AStack
