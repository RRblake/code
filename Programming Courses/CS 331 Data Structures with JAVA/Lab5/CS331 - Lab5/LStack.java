//--------------------------------------------------------------------
//
//  Laboratory 5                                        LStack.jshl
//
//  Definitions for the Linked List implementation of the Stack ADT
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

class LStack implements Stack           // Linked stack class
{
  // Data member
  private StackNode top;               // Reference to top of stack

  // Constructors and helper method setup
  public LStack( )                    // Default Constructor1
  {   top = null;                    }
  public LStack(int size)             // Constructor2: ignore size
                                      // for compatibility with AStack
  {   top = null;                    }
  
  // Class Methods
  private void setup( )               // Called by Constructors only
  {                                  }
  
  // In-lab2 operation
  // Creates a duplicate copy of valueStack
  LStack ( LStack valueStack )          // Copy constructor
  {                                  }
  
  // Insert method clone( ) here
  
  //----- Insert method implementations for the interface Stack here -----//
  
  public void push(Object newElement)     // Push Object onto stack
  {                         
    // make sure newElement isn't null and stack isn't full
    if (newElement == null)
    {
        System.out.println("Element is null -- added nothing to the stack");
        return;
    }

    // add code here to add an element to the top of the stack
	StackNode newNode = new StackNode(newElement, top);
	top = newNode;
	  
  }
  
  public Object pop( )                // Pop Object from top of stack
  { 
    // make sure stack isn't empty
    if(isEmpty( ))
    {
        System.out.println("Empty stack -- removed nothing from stack");
        return null;
    }
    // add code here to remove an element from the top of the stack and
    // return its value
    top = top.getNext();
	return top;
  }
  
  public void clear()
  {
	top = null;
  }
  
  public boolean isEmpty()
  {
	if(top == null)
	{
		return true;
	}
	else
	{
		return false;
	}
  }
  
  public boolean isFull()
  {
	System.out.println("There is no limit to how big we can make a linked list.");
	return false;
  }
  
  public void showStructure ( )
  // Linked list implementation. Outputs the elements in a stack. If
  // the stack is empty, outputs "Empty stack". This operation is
  // intended for testing and debugging purposes only.
  {
    StackNode p;                        // Iterates through the stack

    if (isEmpty( ))
    System.out.println("Stack is empty");
    else
    {
       System.out.print("top ");
       for ( p = top ; p != null ; p = p.getNext( ) )
           System.out.print(p.getElement( ) + " ");
       System.out.println("bottom");
    }
   } // showStructure for LStack
  
} // class LStack
