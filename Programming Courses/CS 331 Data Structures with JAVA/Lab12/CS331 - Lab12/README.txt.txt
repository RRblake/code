---------------------------------
Bridge
---------------------------------
Test #1

Test Case	Commands	Expected Result	Checked

Insert  	+4 +2 +6 +1 +5	   6\		yes

				       5
				4<

				   2\

				       1 	
Retrieve 	?2	Found		yes
Output keys	K	1 2 3 4 5 6	yes
Full or Empty	E F	false false	yes
Clear		C	null		yes
---------------------------------
In-Lab #1
---------------------------------
Test

Test case	Commands	Expected Result	Checked

Insert  	+4 +2 +6 +1 +5	   6\		yes

				        5
				4<

				   2\

					1 
Height		H		Height is 3
													
---------------------------------
Postlab #1
---------------------------------
The tallest height would be N long.
If the input was 1234567
The tree would grow straight downward.

The shortest height would be log N long.
If the input was 4261357
The tree would expand outward as it goes down to fill in the whole tree.