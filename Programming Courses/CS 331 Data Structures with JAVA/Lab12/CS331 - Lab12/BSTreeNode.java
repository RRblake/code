//--------------------------------------------------------------------
//
//  Laboratory 12                                   BSTreeNode.jshl
//
//  (Shell) Class definitions for the linked implementation of the
//  Binary Search Tree ADT -- including the recursive partners of
//  the public member functions
//
//  Note:  the methods that implement an interface MUST BE 
//         declared public
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

class BSTreeNode implements TreeNode
{
    // Data members
    private TreeElem element;         // Binary search tree element
    private TreeNode left,          // Reference to the left child
                    right;          // Reference to the right child

    // Constructor
    public BSTreeNode ( TreeElem elem )
    {
		element = elem;
		left = null;
		right = null;
    }
    
    // Class Methods used by client class
    // methods from interface TreeNode
    
	public TreeNode getLeft()
	{return left;}	

	public TreeNode getRight()
	{return right;}	
	
	public TreeElem getElement()
	{return element;}	
	
	public TreeNode setLeft(TreeNode l)
	{
		left = l;
		return left;
	}	
	
	public TreeNode setRight(TreeNode r)
	{
		right = r;
		return right;
	}	
		
	public TreeElem setElement(TreeElem e)
	{element = e;
		return element;}	
} // class BSTreeNode
