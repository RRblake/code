//--------------------------------------------------------------------
//
//  Laboratory 12                                         BSTree.jshl
//
//  (Shell) Class definitions for the linked implementation of the
//  Binary Search Tree ADT -- including the recursive partners of
//  the public member functions
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------
import java.io.*;
import java.util.*;
import java.lang.*;
class BSTree
{
    // Data member
    private TreeNode root;          // Reference to the root node
    
    // Constructor
    public BSTree ( )
    {
		root = null;
    }
    
    // Binary search tree manipulation methods
    public void insert(TreeElem newElement) {
    BSTreeNode newNode = new BSTreeNode(newElement);
    if (root == null) 
	{
        root = newNode;
    } 
	else 
	{
        insertData(newNode, root);
    }
}

	private void insertData(BSTreeNode node, TreeNode levelNode) 
	{
		if(node.getElement().key() <= (levelNode.getElement().key())) 
		{
			if (levelNode.getLeft() == null) 
			{
				levelNode.setLeft(node);
			} 
			else 
			{
				insertData(node, levelNode.getLeft());
			}
		} 
		else
		{
			if (levelNode.getRight() == null) 
			{
				levelNode.setRight(node);
			} 
			else 
			{
				insertData(node, levelNode.getRight());
			}
		}
	}

    public TreeElem retrieve ( int searchKey )      // Retrieve element
    {
		TreeNode prevNode;
		TreeNode node = root;
		if(isEmpty())
		{
			return null;
		}
		if(root.getElement().key() == searchKey)
		{
			return root.getElement();
		}
		else 
		{
				while ((node.getLeft() != null) || (node.getRight() != null))
				{
					if (node.getElement().key() == searchKey)
					{
						return node.getElement();
					}
					else if (node.getElement().key() > searchKey)
					{
						if (node.getLeft() != null)
						{
	
							node = node.getLeft();
						}
						else
						{
							return null;
						}
					}
					else 
					{
						if (node.getRight() != null)
						{
							node = node.getRight();
						}
						else
						{
							return null;
						}
					}
				}
				if(node.getElement().key() == searchKey)
				{
					return node.getElement();
				}
				else
				{
					return null;
				}
		}
	}
	
	// Couldnt not figure out the algorithm to remove an element.
	
   /* public void remove ( int deleteKey )            // Remove element
    {
		TreeNode node = root;
		TreeNode newNode;
		TreeNode prevNode;
		if(isEmpty())
		{
			return;
		}
		if(root.getElement().key() == deleteKey)
		{
			root = null;
			return;
		}
		else
		{
			while ((node.getLeft() != null) || (node.getRight() != null))
				{
					if (node.getElement().key() == deleteKey)
					{
						newNode = node;
						if ((node.getLeft() != null) && (node.getRight() != null))
						{
							
						}
						else if (node.getRight() == null)
						{
							newNode = node.getLeft();
							while(newNode.getRight() != null)
							{
								newNode = newNode.getRight();
							}
							if (newNode.getLeft() == null)
							{
								if(prevNode.getLeft().getElement().key() == node.getElement().key())
								{
									prevNode.setLeft(node.getRight());
								}
								else
								{
									prevNode.setRight(node.getRight());
								}
							}
								
							
						}
						else if (node.getLeft() == null)
						{
							if(prevNode.getLeft().getElement().key() == node.getElement().key())
							{
								prevNode.setLeft(node.getRight());
							}
							else
							{
								prevNode.setRight(node.getRight());
							}
						}
						else
						{
							if(prevNode.getLeft().getElement().key() == node.getElement().key())
							{
								prevNode.setLeft(null);
							}
							else
							{
								prevNode.setRight(null);
							}
						}
					}
					else if (node.getElement().key() > searchKey)
					{
						if (node.getLeft() != null)
						{
							prevNode = node;
							node = node.getLeft();
						}
						else
						{
							return;
						}
					}
					else 
					{
						if (node.getRight() != null)
						{
							prevNode = node;
							node = node.getRight();
						}
						else
						{
							return;
						}
					}
				}
				if(node.getElement().key() == searchKey)
				{
					return node.getElement();
				}
				else
				{
					return;
				}
		}
		return;
		
    }*/
	

    public void writeKeys ( )                       // Output keys
    {
		if ( root == null )
            System.out.println("Empty tree");
        else
        {
            System.out.print("The keys in the tree are: ");
            recWrite(root);
            System.out.println( );
        }
		
    }
	
	private void recWrite ( TreeNode p)
    // Recursive partner of the showStructure( ) method. Outputs the
    // subtree whose root node is pointed to by p. Parameter level is the
    // level of this node within the tree.
    {
        int j;                                      // Loop counter
        
        if ( p == null )
            return;

        TreeElem q = (TreeElem)p.getElement( );     // Cast Object to TreeElem

        recWrite(p.getLeft( ));          // Output right subtree
       
        System.out.print(" " + q.key( ));           // Output key
        
      
        recWrite(p.getRight( ));           // Output left subtree
    }
	
    public void clear ( )                           // Clear tree
    {
		root = null;
    }

    public boolean isEmpty ( )                      // Is tree empty?
    {
		return (root == null);
    }

    public boolean isFull ( )                       // Is tree full?
    // Java throws an OutOfMemoryError when no more memory remains,
    //   so here false is always naively returned.
    {
		return false;
    }

    public void showStructure ( )
    // Outputs the keys in a binary search tree. The tree is output
    // rotated counterclockwise 90 degrees from its conventional
    // orientation using a "reverse" inorder traversal. This operation is
    // intended for testing and debugging purposes only.
    {
        if ( root == null )
            System.out.println("Empty tree");
        else
        {
            System.out.println( );
            showSub(root, 1);
            System.out.println( );
        }
    }

    //--------------------------------------------------------------------
    //
    //                         In-lab operations
    //
    //--------------------------------------------------------------------

    public void writeLessThan ( int searchKey )     // Output keys
                                                    //   < searchKey
    {
    }

    public int height ( )                           // Height of tree
    {
		if(isEmpty())
		{
			return 0;
		}
		return recHeight(root);
    }
	
	public int recHeight (TreeNode tn)                           // Height of tree
    {
		if (tn == null)
		{
			return 0;
		}
		return (Math.max(recHeight(tn.getLeft()), recHeight(tn.getRight())) + 1);
    }

    // Recursive partners of the public member methods
    // ----- insert these methods here.

    private void showSub ( TreeNode p, int level )
    // Recursive partner of the showStructure( ) method. Outputs the
    // subtree whose root node is pointed to by p. Parameter level is the
    // level of this node within the tree.
    {
        int j;                                      // Loop counter
        
        if ( p == null )
            return;

        TreeElem q = (TreeElem)p.getElement( );     // Cast Object to TreeElem

        showSub(p.getRight( ), level + 1);          // Output right subtree
        for ( j = 0 ; j < level ; j++ )             // Tab over to level
            System.out.print("\t");
        System.out.print(" " + q.key( ));           // Output key
        if ( ( p.getLeft( ) != null ) &&            // Output "connector"
             ( p.getRight( ) != null ) )
            System.out.print("<");
        else if ( p.getRight( ) != null )
            System.out.print("/");
        else if ( p.getLeft( ) != null )
            System.out.print("\\");
        System.out.println( );
        showSub(p.getLeft( ), level + 1);           // Output left subtree
    }

} // class BSTree