//--------------------------------------------------------------------
//
//  Laboratory 6                                          AQueue.jshl
//
//  Class definition for the array implementation of the Queue ADT
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

class AQueue implements Queue               // Array-based queue class
{
    // Data members
    private int maxSize;        // Maximum number of elements in the queue
    private int front;          // Index of the front element
    private int rear;           // Index of the rear element
	private int numElements;    // Number of elements in array
    private Object [] element;  // Array containing the queue elements

    // Constructors
    public AQueue ( )               // Constructor: default size
    { 			
		setup(defMaxQueueSize);		
	}
    public AQueue ( int size )      // Constructor: sets size
    { 
		if(size > 0)
		{
			setup(size);
		}		
	}

    // Class methods
    private void setup(int size)    // Called by Constructors only
    {	
		maxSize = size;
		front = -1;
		rear = 0;
		element = new Object[size];
		numElements = 0;
	}

    // Queue manipulation operations
    //----- Insert method implementations for the interface Queue here -----//
	
	public void enqueue ( Object newElement )
	{
		if(isFull() == true)
		{
			System.out.println("Queue is full. Could not complete operation.");
			return;
		}
		if(isEmpty() == true)
		{
			front++;
			element[front] = newElement;
			numElements++;
			return;
		}
		rear = (rear + 1) % maxSize;
		element[rear] = newElement;
		numElements++;
		
	}
	
	public Object dequeue ( )
	{

		if(isEmpty() == true)
		{
			System.out.println("Queue is empty. Could not complete operation.");
			return null;
		}
		if(numElements == 1)
		{
			Object temp = element[front];
			element[front] = null;
			numElements--;
			front = -1;
			rear = 0;
			return temp;
		}
		Object temp = element[front];
		element[front] = null;
		front = (front + 1) % maxSize;
		numElements--;
		return temp;
	}
	
	public void clear ( )
	{
		if(isEmpty() == true)
		{
			System.out.println("Queue is empty. Could not complete operation.");
			return;
		}
		
		for(int i = 0; i < maxSize; i++)
		{
			element[i] = null;
		}
		numElements = 0;
		front = -1;
		rear = 0;
	}
	
	public boolean isEmpty ( )
	{		
		return (numElements == 0);
	}
	
	public boolean isFull ( )
	{
		return (numElements == maxSize);
	}
    public void showStructure ( )
    // Array implementation. Outputs the elements in a queue. If the
    // queue is empty, outputs "Empty queue". This operation is intended
    // for testing and debugging purposes only.
    {
        int j;   // Loop counter

        if ( front == -1 )
            System.out.println("Empty queue");
        else
        {
            System.out.println("front = " + front + "  rear = " + rear);
            for ( j = 0 ; j < maxSize ; j++ )
                System.out.print(j + "\t");
            System.out.println( );
            if ( rear >= front )
                for ( j = 0 ; j < maxSize ; j++ )
                    if ( ( j >= front ) && ( j <= rear ) )
                        System.out.print(element[j] + "\t");
                    else
                        System.out.print(" \t");
            else
                for ( j = 0 ; j < maxSize ; j++ )
                    if ( ( j >= front ) || ( j <= rear ) )
                        System.out.print(element[j] + "\t");
                    else
                        System.out.print(" \t");
            System.out.println( );
        }
    } // showStructure for AQueue

    // In-lab operations
    // These methods are NOT included in the interface Queue.
    // Since compiler errors will occur before some of these methods
    //   are implemented, they have been temporarily commented out.
    // Remove the comment delimeters '/*' and '*/' once these methods
    //   are implemented.
   
    public void putFront ( Object newElement )  // Insert at front
    {	
		if(isFull() == true)
		{
			System.out.println("Queue is full. Could not complete operation.");
			return;
		}
		if(newElement == null)
		{
			return;
		}
		if(front == -1)
		{
			front++;
			element[front] = newElement;
			numElements++;
			return;
		}
		if(front == 0)
		{
			front = maxSize - 1;
			element[front] = newElement;
			numElements++;
			return;
		}
		front--;
		element[front] = newElement;
		numElements++;
	}
    public Object getRear ( )                   // Get from rear
    {  
		if(isEmpty() == true)
		{
			System.out.println("Queue is empty. Could not complete operation.");
			front = -1;
			rear = 0;
			return null;
		}
		if(numElements == 1)
		{
			Object temp = element[rear];
			element[rear] = null;
			numElements--;
			front = -1;
			rear = 0;
			return temp;
		}
		if(rear == 0)
		{
			
			Object temp = element[rear];
			element[rear] = null;
			numElements--;
			rear = maxSize - 1;
			return temp;
		}
		
		Object temp = element[rear];
		element[rear] = null;
		rear--;
		numElements--;
		return temp;
	}
    public int length ( )                       // Number of elements
    {       
		return numElements;
	}


 } // class AQueue