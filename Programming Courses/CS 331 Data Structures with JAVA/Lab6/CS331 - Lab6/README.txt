---------------------------------
Bridge
---------------------------------
Test

Test Case		Commands	Expected Result	Checked

Series of enqueues	+a +b +c +d	A b c d		yes
Series of dequeues	- - -		D		yes
More enqueues		+e +f		D e f		yes
More dequeues		- - 		F		yes
Empty? Full?		E F		false false	yes
Empty the queue		-		Empty Queue	yes
Empty? Full?		E F		true false	yes
---------------------------------
In-Lab #1
---------------------------------
Test

Test case			Commands	Expected Result	Checked

Series of calls to putFront	>a >b >c >d	D c b a		yes
Series of calls to getRear	= = =		D		yes
More calls to putFront		>e >f		F e d		yes
More calls to getRear		= =		F		yes																
---------------------------------
Postlab #1
---------------------------------
Part A
In the array representation, when you create it it automatically takes up all the memory
needed to store the data you need to store. It creates slots as if the integers you put in
will take up all 4 bytes, when in some cases you won't take up all 4 bytes. The linked list
uses up memory as you store in data and it only takes up memory of the size of the integer
that you put in. So the linked list will use less memory then an array representation.

Part B
For the array representation, it is using the full 100% of each of the queues even though all
of them are partially filled. The linked list is only using the amount that is filled up.
So the linked list is only 260% full while the array is 1000% full.


