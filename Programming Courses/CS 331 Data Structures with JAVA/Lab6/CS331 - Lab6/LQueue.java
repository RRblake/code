class LQueue implements Queue
{
    // Data members
    private QueueNode front,        // Reference to the front element
                       rear;        // Reference to the rear element

    // Constructors
    public LQueue ( )               // Constructor: default
    {      
		setup();
	}
    public LQueue ( int size )      // Constructor: ignore size
    {   
		setup();
	}
    
    // Class methods
    private void setup( )           // Called by Constructors only
    {   
		front = null;
		rear = null;
	}           // Initializes front and rear to null
    
    //----- Insert method implementations for the interface Queue here -----//
	
	public void enqueue ( Object newElement )
	{
		QueueNode newNode = new QueueNode(newElement);
		if(rear == null)
		{
			front = newNode;
		}
		else
		{
			rear.setNext(newNode);
		}
		rear = newNode;
	}
	
	public Object dequeue ( )                 // Dequeue element from front
	{
		if(isEmpty())
		{
			System.out.println("Queue is empty. Could not complete operation.");
			return null;
		}
		Object element = front.getElement();
		front = front.getNext();
		if(front == null)
		{
			rear = null;
		}
		return element;
		
	}
	
    public void clear ( )                      // Remove all elements from queue
	{
		front = null;
		rear = null;
	}
    
    // Queue status operations
    public boolean isEmpty ( )                 // Is Queue empty?
	{
		return ((front == null) && (rear == null));
	}
	
    public boolean isFull ( )					// Is Queue full?
	{
		System.out.println("No limit on size for linked lists.");
		return false;
	}
    public void showStructure ( )
    // Linked list implementation. Outputs the elements in a queue. If
    // the queue is empty, outputs "Empty queue". This operation is
    // intended for testing and debugging purposes only.
    {
        QueueNode p;                // Iterates through the queue

        if ( front == null )
            System.out.println("Empty queue");
        else
        {
            System.out.print("front ");
            for ( p = front ; p != null ; p = p.getNext( ) )
                System.out.print(p.getElement( ) + " ");
            System.out.println("rear");
        }
    } // showStructure for LQueue

} // Class LQueue


