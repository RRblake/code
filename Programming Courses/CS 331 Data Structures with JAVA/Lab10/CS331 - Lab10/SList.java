//--------------------------------------------------------------------
//
//  Laboratory 7                                         SList.jshl
//
//  Class definitions for the singly linked list implementation of 
//  the List ADT
//
//  The student is to complete all missing or incomplete method 
//     implementations for each class (Slist and SListNode)
//
//--------------------------------------------------------------------

class SList implements List
{
    // Data members
    public SListNode head,     // Reference to the beginning of the list
        cursor; // Reference to current cursor position
    public int cursorSpot;
    // Constructors & Helper Method
    SList (  )                  // Default constructor: Creates an empty list
    {
		setup();
    }

    SList ( int size )
        // Creates an empty list. The argument is included for compatibility
        // with the array implementation; size is ignored.
    {
		setup();
    }

    // Class Methods    
    private void setup( )   // Called by constructors only: Creates an empty list
    {
		head = null;
		cursor = null;
		cursorSpot = 0;
    }


    // ----- Insert method definitions for the interface List here ------ //
	
	public void insert(Object newElement)
	{
		SListNode newNode = new SListNode(newElement);
		if(head == null)
		{
			head = newNode;
			cursor = head;
			cursorSpot = 0;
			return;
		}
		else
		{
			if(cursor.getNext() == null)
			{
				cursor.setNext(newNode);
				cursorSpot++;
				cursor = newNode;
				return;
			}

			newNode.setNext(cursor.getNext());
			cursor.setNext(newNode);
			cursorSpot++;
			
		}
		
		cursor = newNode;
	}
	
	public void remove( )
	{
		SListNode temp;
		if(isEmpty() == true)
		{
			System.out.println("List is empty.");
		}
		if(head.getNext() == null)
		{
			head = null;
			cursor = null;
			return;
		}
		if(cursorSpot == 0)
		{
			head = cursor.getNext();
			cursor = head;
			return;
		}
		if(cursor.getNext() == null)
		{
			gotoPrior();
			cursor.setNext(null);
			cursor = head;
			cursorSpot = 0;
		}
		else
		{
			temp = cursor.getNext();
			gotoPrior();
			cursor.setNext(temp);
			cursorSpot++;
			cursor = cursor.getNext();
		}
		
		
		
		
	}
	
	public void replace (Object newElement)
	{
		cursor.setElement(newElement);
	}
	
	public void clear( )
	{
		 head = null;
		 cursor = null;
	}
	
	public boolean isEmpty( )
	{
		return (head == null);
	}
	
	public boolean isFull( )
	{
		System.out.println("Linked lists don't have a limited size.");
		return false;
	}

    public boolean gotoBeginning( )          
    {
		cursor = head;
		cursorSpot = 0;
		return true;
	}
	
    public boolean gotoEnd( )
    {
		while(cursor.getNext() != null)
		{
			cursor = cursor.getNext();
			cursorSpot++;
		}
		return true;
	}
	
    public boolean gotoNext( )
    {
		if(cursor.getNext() == null)
		{
			return true;
		}
		cursor = cursor.getNext();
		cursorSpot++;
		return true;
	} 

    public boolean gotoPrior( )   
    {
		if(cursor == head)
		{
			return true;
		}
		SListNode target = cursor;
		cursor = cursor.getNext();
		if(cursor == null)
		{
			cursor = head;
		}
		while(cursor.getNext() != target)
		{
			cursor = cursor.getNext();
			if(cursor == null)
			{
				cursor = head;
			}
		}
		cursorSpot--;
		return true;
	}

    public Object getCursor( )
	{
		Object element = cursor.getElement();
		return element;
	}

	public void showStructure( )
	{
		SListNode p;                // Iterates through the List

        if ( head == null )
            System.out.println("Empty queue");
        else
        {
			System.out.println("Cursor Points to: "+cursor.getElement());
			int i = 0;
			for ( p = head ; p != null ; p = p.getNext( ) )
			{
				
				System.out.print(i + "\t");
				i++;
			}
 
            System.out.println();
            for ( p = head ; p != null ; p = p.getNext( ) )
                System.out.print(p.getElement( ) + "\t");
            System.out.println();
        }
	}
	
    //--------------------------------------------------------------------
    //
    //                        In-lab operations
    //
    //--------------------------------------------------------------------

    void moveToBeginning ( )                    // Move to beginning
    {
		SListNode temp1, temp2;
		
		temp1 = cursor.getNext();
		temp2 = new SListNode(cursor.getElement());
		gotoPrior();
		cursor.setNext(temp1);
		cursorSpot = 0;
		temp2.setNext(head);
		head = temp2;
		cursor = head;
    }

    void insertBefore ( Object newElement )     // Insert before cursor
    {

    }
    
} // class SList