---------------------------------
Pre-Lab
---------------------------------

----------------
Part A
----------------
5.
0 1 2
a b c
List : a
b
c

6.
0 1 2 3
a b c !

8.
Empty Queue
List:

9.
0
!

----------------
Part B
----------------
4.
Mirror : abccba

5.
Prints a, starts method again wih b, finishes b method, prints a again, and finishes a method.

6.
It is the base case for this recursive method.

7.
print a -> call b -> print b -> call c -> print c -> print c -> print b -> print a

----------------
Part c
----------------
4.
0 1 2
c b a

5.
It goes through the list to the last element and makes it the head and then finishes the calls by reversing the direction.

6.
It is the base case for this method.

7.
null, a -> a, b -> b, c -> c, null -> c = head -> b -> a

----------------
Part D
----------------
4.
0 1
a b

5.
It is the general case of the recursive method.

6.
a -> b -> c -> null
a -> b -> null

7.
NullPointerException gets thrown because the method requires at least 2 elements in order for it to work correctly.

----------------
Part E
----------------
3.
Length = 3

4.
It is the base case.

5.
a, count+1 -> b, count+1 -> c, count+1 -> null, count -> return count

6.
null, basecase equals count to 0 -> return 0

---------------------------------
Bridge
---------------------------------

----------------
Part A
----------------

3.
acedb

4.
Prints a, calls with b, once method with b finishes it prints next element which is b.

5.
a -> c -> e, base case -> d -> b

----------------
Part B
----------------

3.
0 1 2
a c e

4.
p = a
q = a
p = b
a -> c
b -> a

5.
a -> c -> e, base case

---------------------------------
In-Lab Exercise 1
---------------------------------

----------------
Part A
----------------

Test Case	List	Expected Result	Checked

Reverse List	abcde	edcba		yes
		ace	eca		yes
		cbdea	aedbc		yes

----------------
Part B
----------------

Test Case	List	Expected Result	Checked

Mirror List	abcde	abcdeedcba	yes
		ace	aceeca		yes
		cbde	cbdeedbc	yes

---------------------------------
In-Lab Exercise 1
---------------------------------
Changing the if method to a while method would cause it to go into an infinite loop.

