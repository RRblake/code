import java.util.*;
import java.io.*;

public class Lexical
{
	public static void main(String[] args)
	{
		int count = 1;
		String filename = "progsamp.dat";
		FileInputStream inFile;
		try
		{
			inFile = new FileInputStream(filename);
			BufferedReader bufFinReader = new BufferedReader(new InputStreamReader(inFile));
			String line = bufFinReader.readLine();
			while(!(line.equals(null)))
			{
				
				StringTokenizer strtok = new StringTokenizer(line, " ");
				while(strtok.hasMoreTokens())
				{
					String token = strtok.nextToken();
					System.out.println(count+": "+token);
					count++;
				}
				line = bufFinReader.readLine();
			}
			inFile.close();
		}
		catch (FileNotFoundException e)
		{
			System.out.print("Error opening file "+filename);
		}
		catch (IOException IOE)
		{
			System.out.print("Something went wrong.");
		}
		catch (NullPointerException NPE)
		{
			System.out.println("End of reading file.");
		}
	}
}