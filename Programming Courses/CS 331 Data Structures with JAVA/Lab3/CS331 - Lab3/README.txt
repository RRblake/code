---------------------------------
Bridge
---------------------------------
Test1:

Test case		String		Expected Result		Checked

Simple string		alpha		0 1 2 3 4		yes
					a l p h a
Longer string		epsilon		0 1 2 3 4 5 6		yes
					e p s i l o n
Single-char		a		0			yes
					a
Empty string		empty					yes

Test2:

Test case		Pair of Strings		Expected Result < == >	Checked

Second string greater	alpha epsilon		T F F				yes
First string greater	epsilon alpha		F F T				yes
Identical Strings	alpha alpha		F T F				yes
First string embedded	alp alpha		T F F				yes
Second string embedded	alpha alp		F F T				yes
First string in char	a alpha			T F F				yes
Second string in char	alpha a			F F T				yes
First string is empty	empty alpha		T F F				yes
Second string is empty	alpha empty		F F T				yes
Both strings are empty	empty empty		F T F				yes

Test3:

Test case	Char to find	Expected count	Checked

a		z		0		yes
a		a		1		yes
mississippi	i		4		yes
Empty String	empty		0		yes

Test4:

Test case		(String, start, count)	Expected result	Checked

simple substring	("alpha", 0, 3)		alp		yes
Single char string	("a", -1, 1)		a		yes	
Start > String		("test", 5, 1)				yes
Count > String		("test", 0, 8)				yes
Start and Count >	("test", 8, 8)				yes

Test5:

Test case	Lines printed		Expected Result		Checked

Simple Phrase	String is:		Begin and end		yes
"Begin and end"	The string's length is:	13		
		The first letter is:	B
		The last letter is:	d
		The first word is:	Begin
Simple Word	String is:		Begin			yes
"Begin"		The string's length is:	5
		The first letter is:	B
		The last letter is:	d
		The first word is:	Begin
Single char	String is:		A			yes
string "A"	The string's length is:	1
		The first letter is:	A
		The last letter is:	A
		The first word is:	A
Empty string	String is:					yes
		The string's length is:	0
		The string is empty!	No more data to print! 
---------------------------------
In-Lab #1
---------------------------------
Test for Lexical Analysis Program:

Test case:
class Progsamp
{
    public static void main ( String args[ ] )
    {
        int j ,
            total = 0 ;
        for ( j = 1 ; j <= 20 ; j ++ )
            total += j ;
    }
}

Expected result:
1: class
2: Progsamp
3: {
4: public
5: static
6: void
7: main
8: (
9: String
10: args[
11: ]
12: )
13: {
14: int
15: j
16: ,
17: total
18: =
19: 0
20: ;
21: for
22: (
23: j
24: =
25: 1
26: ;
27: j
28: <=
29: 20
30: ;
31: j
32: ++
33: )
34: total
35: +=
36: j
37: ;
38: }
39: }
End of reading file.

---------------------------------
Postlab #1
---------------------------------
In the first case, everything you expect to happens, happens. In the second case bstr2 
actually makes a hidden change. The changes of str1 do NOT affect str2. The changes of 
bstr1 do affect bstr2. No it is not the same because the first scenario we aren't 
creating string objects. Where in the second scenario we are doing that, and the "labels" 
point to areas in memory instead of actually holding that value. So when we say 
[bstr2 = bstr1], were actually making bstr2 point at the same object as bstr1.
