import java.util.*;
import java.io.*;
public class CouponGet
{

	public static void main(String [] args)
	{	
		Scanner scanin = new Scanner(System.in);
  
        SList sortedCouponList = new SList();  //sorted list
		ListArray couponList;
		System.out.print("\nWould you like to load coupons? (y or n): ");
		String in = scanin.nextLine();
		if (in.equals("y"))
		{	
			System.out.print("\nDo you want default sized directory?\nNOTE:(If y is chosen then the size will be 30.): ");
			String size = scanin.nextLine();
			if(size.equals("y"))
			{
				couponList = new ListArray(30); // default coupon list
			}
			else
			{
				System.out.print("How big do you want the directory to be: ");
				int yourSize = scanin.nextInt();
				couponList = new ListArray(yourSize);// coupon list
			}
			System.out.println("Loading Coupons...");
			try
			{
				FileInputStream fis = new FileInputStream("Coupons.ser");
				
				ObjectInputStream ois = new ObjectInputStream(fis);
				while(true)
				{
					try
					{
						Coupon cin = (Coupon)ois.readObject();
						couponList.insert(cin);
					}
					catch (EOFException eof)
					{
						System.out.println("All of the coupons have been loaded. Starting service!");
						break;
					}
					
				}
				ois.close();
			}
			catch (FileNotFoundException  FNFE)
			{
				System.out.println("File not found.");
			}
			catch (IOException ioe)
			{
				System.out.println("Something went wrong." + ioe);
			}
			catch (ClassNotFoundException CNFE)
			{
				System.out.println("Something went wrong." + CNFE);
			}
		}
		else
		{
			System.out.println("Goodbye.");
			return;
		}

		
     
        System.out.println("\n----------------------------------------\nWelcome to the Coupon Retrieval Service!\n----------------------------------------");
		char cmd = 'a';
        System.out.println("\nType a command below:");
        System.out.println("  I : Insert a coupon into the directory.");
        System.out.println("  L : Locate a specific coupon from the directory by the product name.");
        System.out.println("  P : Print unsorted list of coupons.");
		System.out.println("  V : View the sorted coupon list by variable of your choice.");
		System.out.println("  M : Print the menu again to see commands.");
        System.out.println("  Q : Quit the program");
        System.out.println();
		
        do
        {
		try
		{
		
            System.out.print("----------------------------------------\nCommand: ");          // Read command
            cmd = (char)System.in.read();
            while (Character.isWhitespace(cmd))     // ignore whitespace
                cmd = (char)System.in.read(); 
            
            
            switch ( cmd )
            {
                case 'I' :                          // Insert
					Scanner scan1 = new Scanner(System.in);
					
                    System.out.println("Inserting a coupon...");
					System.out.println();
					
					System.out.print("Who is the provider: ");
					String test = scan1.nextLine();
					String prov = scan1.nextLine();
					System.out.print("What is the product: ");
					String prod = scan1.nextLine();
					System.out.print("What is the price: ");
					double pri = scan1.nextDouble();
					System.out.print("What is the discount rate (example: 20, 55, 10): ");
					double dis = scan1.nextDouble();
					System.out.print("What is the expiration date: ");
					int exp = scan1.nextInt();
					System.out.print("Has this coupon been redeemed already (true or false): ");
					boolean red = scan1.nextBoolean();
					Coupon c = new Coupon(prov, prod, pri, dis/100, exp, red);				
                    couponList.insert(c);
					System.out.println("Coupon has been inserted successfully, thank you.");
                    break;

                case 'L' :                // clear				
					Scanner scan2 = new Scanner(System.in);
					
					System.out.print("What product are you looking for: ");
					String garb = scan2.nextLine();
					String prod1 = scan2.nextLine();
					System.out.println("Locating a coupon...");
					Coupon found = null;
					int count1 = 0;
					Coupon [] temp = couponList.sortProduct();
					int count2 = 0;
					for(int a = 0; temp[a] != null; a++)
					{
						count2++;
					}
					for(int i = 0; i < count2; i++ )
					{
						sortedCouponList.insert(temp[i]);

					
					
						count1++;
						if(sortedCouponList.getCursor().getProduct().equals(prod1))
						{
							found = sortedCouponList.getCursor();
							break;
						}
					}
				
					if (found != null)
					{
						System.out.println("\nThe coupon has been found with "+count1+" comparisons.");
						System.out.println("\nHere is the coupon:\n"+found.toString());
					}
					else
					{
						System.out.println("No coupon is found within the list.");
					}
					
					
					break;

                case 'P' :               
					couponList.showStructure();
                    break;
				
				case 'V' :  
					Scanner scanChoice = new Scanner(System.in);
					System.out.println("How do you want to view the coupons (Ex: Final Price, Status, or Product): ");
					String junk = scanChoice.nextLine();
					String choice = scanChoice.nextLine();
					
					if(choice.equals("Product"))
					{
						Coupon [] temp1 = couponList.sortProduct();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					else if(choice.equals("Final Price"))
					{
						Coupon [] temp1 = couponList.sortFinalPrice();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					else if(choice.equals("Provider"))
					{
						Coupon [] temp1 = couponList.sortProvider();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					if(choice.equals("Price"))
					{
						Coupon [] temp1 = couponList.sortPrice();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					if(choice.equals("Discount"))
					{
						Coupon [] temp1 = couponList.sortDiscount();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					if(choice.equals("Status"))
					{
						Coupon [] temp1 = couponList.sortStatus();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
					
					if(choice.equals("Expiration"))
					{
						Coupon [] temp1 = couponList.sortExpiration();
						int count = 0;
						for(int a = 0; temp1[a] != null; a++)
						{
							count++;
						}
						for(int i = 0; i < count; i++ )
						{
							sortedCouponList.insert(temp1[i]);
							System.out.println(sortedCouponList.getCursor().toString());
						}
					}
						
						
		
					
					
                    break;
				case 'M' :
					
					System.out.println("\n----------------------------------------\n\nType a command below:");
					System.out.println("  I : Insert a coupon into the directory and save it into the file.");
					System.out.println("  L : Locate a specific coupon from the directory by the product name.");
					System.out.println("  P : Print unsorted list of coupons.");
					System.out.println("  V : View the sorted coupon list by variable of your choice.");
					System.out.println("  M : Print the menu again to see commands.");
					System.out.println("  Q : Quit the program");
					System.out.println();
                case 'Q' : case 'q' :               // Quit test program
                    break;

                default :                           // Invalid command
                    if (!(Character.isWhitespace(cmd))) 
                        System.out.println("Inactive or invalid command");
            } // switch
		}
		catch (IOException IOE)
		{
			System.out.println("Something went wrong.");
		}
        } while ( cmd != 'Q'  &&  cmd != 'q' );

    } // main
	
}