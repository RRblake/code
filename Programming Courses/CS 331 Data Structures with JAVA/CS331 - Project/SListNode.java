

class SListNode
{
    // Data members
    private static Coupon element;         // List element
    private SListNode next;         // Reference to the next element
    
    // Constructor
    SListNode ( Coupon elem)
        // Creates a list node containing element elem and next pointer
        // nextPtr.
    {
		element = elem;

    }
    
    // Class Methods used by client class
    SListNode getNext( )                    // Return reference to next element
    {
		return next;
    }
    
    SListNode setNext( SListNode nextVal )  // Set reference to next element
    {                                       //  & return that reference
		next = nextVal;
		return next;
    }
    
   public static Coupon getElement( )             // Return the element in the current node
    {
		return element;
    }
    
    void setElement(Coupon newElem)         // Set current element to newElem 
    {                                 
		element = newElem;
    }
    
} // class SListNode
