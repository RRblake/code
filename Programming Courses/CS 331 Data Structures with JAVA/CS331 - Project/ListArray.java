

class ListArray implements List
{
    // Data Members
	public static int count = 0;
    private int size,           // Actual number of elements in the list
                cursor;         // Cursor array index
    private Coupon [] element;  // Array containing the list elements
                                // As type Coupon, they are 'generic' 

    // Constructors and helper method setup
    public ListArray( )             // Constructor: default size
    {
		setUp (DEF_MAX_LIST_SIZE);
		size = 0;
		cursor = 0;
    }

    public ListArray(int maxNumber) // Constructor: specific size
    {
		if (maxNumber > 0)
		{
			setUp (maxNumber);
			size = 0;
			cursor = 0;
		}
    }

    // Class methods
    private void setUp (int maxNumber)  // Called by constructors only
    {
		element = new Coupon[maxNumber];
    }

    // ------ Insert method implementations for the interface List here ------ //
	public void insert(Coupon newElement)
	{
		
		
		if (isEmpty() == true)
		{
			element[0] = newElement;
			cursor = 0;
		}
		else 
		{
			for(int temp = cursor + 1; temp < size;temp++)
			{
				element[temp + 1] = element[temp];
			}
			element[cursor + 1] = newElement;
			cursor++;
		}
		
		size++;
	}
	public void remove( )
	{
		if ((isFull()== true)&&(cursor == size - 1))
		{
			element[cursor] = null;
			cursor--;
		}
		else
		{
			for(int temp = cursor; temp < size;temp++)
			{
				element[temp] = element[temp + 1];
			}
		}
		
		size--;
		if(cursor == size)
		{
			cursor--;
		}
		else if (cursor == 0)
		{
			cursor = 0;
		}
		
	}
	
	public void replace (Coupon newElement)
	{
		element[cursor] = newElement;
	}
	
	public void clear( )
	{
		for (int i = 0; i < element.length; i++)
		{
			element[i] = null;
			size = 0;
		}
	}
	public boolean isEmpty( )
	{
		if (size == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isFull( )
	{
		if (size == element.length)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean gotoBeginning( )
	{
		if(isEmpty() == false)
		{
			cursor = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoEnd( )
	{
		if(isEmpty() == false)
		{
			cursor = size - 1;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoNext( )
	{
		if (cursor < element.length - 1)
		{
			cursor++;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoPrior( )
	{
		if (cursor > 0)
		{
			cursor--;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Coupon getCursor( )
	{
		return element[cursor];
	}
	
	public int size()
	{
		return size;
	}
    
    public void showStructure ( )
        // Outputs the elements in a list. If the list is empty, outputs
        // "Empty list". This operation is intended for testing/debugging
        // purposes only.

    {
        int j;   // Loop counter

        if ( size == 0 )
            System.out.println("Empty list");
        else
        {
           
            for ( j = 0 ; j < size ; j++ )
                System.out.println(element[j] + "\n");
            System.out.println( );
        }
    }

    //--------------------------------------------------------------------
    //
    //                        In-lab operations
    //
    //--------------------------------------------------------------------

    public void moveToNth ( int n )         // Move element to position n
    {

    }

    public Coupon find ( String findProd )    // Find searchElement
    {
		int start = 0;
		int end = element.length - 1;
		int count;
		while (end > start)
		{
			count = count();
			int middle = (end - start)/2;
			if(element[middle].getProduct() == findProd)
			{
				return element[middle];
			}
			else if (element[middle].getProduct().compareTo(findProd) < 0)
			{
				end = middle - 1;
			}
			else
			{
				start = middle + 1;
			}
		}
		return null;
    }
	
	public int count()
	{
		count++;
		return count;
	}
	public void resetCount()
	{
		count = 0;
	}
	
	public Coupon[] sortProvider()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getProvider().compareTo(element[j].getProvider()) > 0)
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	
	public Coupon[] sortProduct()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getProduct().compareTo(element[j].getProduct()) > 0)
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		
		return element;
	}
	
	public Coupon[] sortPrice()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getPrice() > element[j].getPrice())
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	
	public Coupon[] sortFinalPrice()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getFinalPrice() > element[j].getFinalPrice())
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	
	public Coupon[] sortDiscount()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getDiscount() > element[j].getDiscount())
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	
	public Coupon[] sortExpiration()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if (element[i].getExpiration() > element[j].getExpiration())
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	
	public Coupon[] sortStatus()
	{
		int count = 0;
		for(int a = 0; element[a] != null; a++)
		{
			count++;
		}

		Coupon temp = null;
		for(int i = 0; i < count - 1; i++)
		{
			temp = null;
			for(int j = i + 1; j < count; j++)
			{
				if ((element[i].getStatus() == true) && (element[j].getStatus() == false))
				{

					temp = (element[i]);
					element[i] = (element[j]);
					element[j] = (temp);
				}
			}
		}
		return element;
	}
	

    
} // class ListArray