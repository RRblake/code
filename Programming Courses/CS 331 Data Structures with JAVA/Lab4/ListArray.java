//--------------------------------------------------------------------
//
//  Laboratory 4                                       ListArray.jshl
//
//  Class definition for the array implementation of the List ADT
//
//  The student is to complete all missing or incomplete method 
//     implementations for this class
//
//--------------------------------------------------------------------

class ListArray implements List
{
    // Data Members
    private int size,           // Actual number of elements in the list
                cursor;         // Cursor array index
    private Object [] element;  // Array containing the list elements
                                // As type Object, they are 'generic' 

    // Constructors and helper method setup
    public ListArray( )             // Constructor: default size
    {
		setUp (DEF_MAX_LIST_SIZE);
		size = 0;
		cursor = 0;
    }

    public ListArray(int maxNumber) // Constructor: specific size
    {
		if (maxNumber > 0)
		{
			setUp (maxNumber);
			size = 0;
			cursor = 0;
		}
    }

    // Class methods
    private void setUp (int maxNumber)  // Called by constructors only
    {
		element = new Object[maxNumber];
    }

    // ------ Insert method implementations for the interface List here ------ //
	public void insert(Object newElement)
	{
		
		
		if (isEmpty() == true)
		{
			element[0] = newElement;
		}
		else
		{
			element[cursor] = newElement; 
			cursor++;
		}
		size++;
	}
	public void remove( )
	{
		element[cursor] = null;
		cursor++;
		size--;
	}
	
	public void replace (Object newElement)
	{
		element[cursor] = newElement;
	}
	
	public void clear( )
	{
		for (int i = 0; i < element.length; i++)
		{
			element[i] = null;
			size = 0;
		}
	}
	public boolean isEmpty( )
	{
		if (size == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isFull( )
	{
		if (size == element.length)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public boolean gotoBeginning( )
	{
		if(isEmpty() == false)
		{
			cursor = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoEnd( )
	{
		if(isEmpty() == false)
		{
			cursor = element.length - 1;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoNext( )
	{
		if (cursor < element.length - 1)
		{
			cursor++;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean gotoPrior( )
	{
		if (cursor > 0)
		{
			cursor--;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Object getCursor( )
	{
		return element[cursor];
	}
    
    public void showStructure ( )
        // Outputs the elements in a list. If the list is empty, outputs
        // "Empty list". This operation is intended for testing/debugging
        // purposes only.

    {
        int j;   // Loop counter

        if ( size == 0 )
            System.out.println("Empty list");
        else
        {
            System.out.println("size = " + size + "   cursor = " 
                               + cursor);
            for ( j = 0 ; j < element.length ; j++ )
                System.out.print(j + "\t");
            System.out.println( );
            for ( j = 0 ; j < size ; j++ )
                System.out.print(element[j] + "\t");
            System.out.println( );
        }
    }

    //--------------------------------------------------------------------
    //
    //                        In-lab operations
    //
    //--------------------------------------------------------------------

    public void moveToNth ( int n )         // Move element to position n
    {

    }

    public boolean find ( Object searchElement )    // Find searchElement
    {

    }
    
} // class ListArray