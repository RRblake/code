---------------------------------
Bridge
---------------------------------
Test plan:

Test case		Commands	Expected Result		Checked

Insert at end		+a +b +c +d	a b c D			yes
Travel from beginning	< N N		a b C d			yes
Travel from end		> P P		a B c d			yes
Delete middle element	-		a C d			yes
Insert in middle	+e +f +f	a c e f F d		yes
Remove last element	> -		a c e f F		yes
Remove first element	< -		C e f f			yes
Display element		@		Returns c		yes
Clear the list		C		Empty list		yes

---------------------------------
In-Lab #1
---------------------------------
Test plan:

Test case		DNA Sequence	Expected Result Checked

Sequence with 10 bases	AGTACATGTA	aCount = 4	yes
					cCount = 1
					tCount = 3
					gCount = 2
Sequence with  4 bases	ACTA		aCount = 2	yes
					cCount = 1
					tCount = 1
					gCount = 0
Sequence with 12 bases	ACTAAGTCCTAG	aCount = 4	yes
					cCount = 3
					tCount = 3
					gCount = 2
---------------------------------
Postlab #1
---------------------------------
Insert O(N)
For the insert command it needs to move the list down in order to put in an element.

Remove O(N)
The remove command has the same reasoning behind the Insert command.

gotoNext O(1)
It only need to move the cursor. No need for looping.

gotoPrior O(1)
Same reasoning behind the gotoNext command.


