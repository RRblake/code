class DList implements List     //Circular, doubly linked list implementation of the
                                //            List ADT
{
    // Data members
    private DListNode head,      // Reference to the beginning of the list
                     cursor;    // Reference to current cursor position
    
    // Constructors & Helper Method
    public DList( )             // Default constructor: Creates an empty list
    {  
		setup();
	}
    public DList( int size )    // Constructor: Creates an empty list, size is ignored
    {         
		setup();
	}
    
    // Class methods
    private void setup( )
    // Called by constructors only: Creates an empty list
    {    
		head = null;
		cursor = null;
	}
    
    //----- Insert method definitions for the interface List here -----//   
    public void insert(Object newElement)
	{
		DListNode newNode = new DListNode(newElement);
		if(head == null)
		{
			
			newNode.setNext(newNode);
			newNode.setPrior(newNode);
			head = newNode;
			cursor = head;
			
			return;
		}
		else
		{
			

			newNode.setNext(cursor.getNext());
			newNode.setPrior(cursor);
			cursor.getNext().setPrior(newNode);
			cursor.setNext(newNode);
			
			
			
		}
		
		cursor = newNode;
	}
	
	public void remove( )
	{

		if(isEmpty() == true)
		{
			System.out.println("List is empty.");
			return;
		}
		if(head.getNext() == head)
		{
			head = null;
			cursor = null;
			return;
		}
		if(cursor == head)
		{
			head.getNext().setPrior(head.getPrior());
			head.getPrior().setNext(head.getNext());
			head = cursor.getNext();

			cursor = head;
			return;
		}
		if(cursor.getNext() == head)
		{
			cursor.getNext().setPrior(cursor.getPrior());
			cursor.getPrior().setNext(head);
			cursor = cursor.getPrior();
		}
		else
		{
			cursor.getNext().setPrior(cursor.getPrior());
			cursor.getPrior().setNext(cursor.getNext());
			cursor = cursor.getPrior();
						
		}		
	}
	
	public void replace (Object newElement)
	{
		cursor.setElement(newElement);
	}
	
	public void clear( )
	{
		 head = null;
		 cursor = null;
	}
	
	public boolean isEmpty( )
	{
		return (head == null);
	}
	
	public boolean isFull( )
	{
		System.out.println("Linked lists don't have a limited size.");
		return false;
	}

    public boolean gotoBeginning( )          
    {
		cursor = head;
		
		return true;
	}
	
    public boolean gotoEnd( )
    {
		cursor = head.getPrior();
		
		return true;
	}
	
    public boolean gotoNext( )
    {
		cursor = cursor.getNext();
		return true;
	} 

    public boolean gotoPrior( )   
    {
		cursor = cursor.getPrior();
		return true;
	}

    public Object getCursor( )
	{
		Object element = cursor.getElement();
		return element;
	}
    public void showStructure ( ) 
    // Outputs the elements in a list. If the list is empty, outputs
    // "Empty list". This operation is intended for testing and
    // debugging purposes only.
    {
        DListNode p;   // Iterates through the list

        if ( head == null )
            System.out.println("Empty list");
        else
        {
            p = head;
            do
            {
                if ( p == cursor )
                    System.out.print("[" + p.getElement( ) + "] ");
               
                else
				{
					System.out.print(p.getElement( ) + " ");
				}
                p = p.getNext( );
            } while ( p != head );
            System.out.println( );
        }
    }

    // In-lab opertions -- not declared in interface List
    public void reverse ( )
    // Reverses the order of the elements in a list. The cursor does
    // not move. 
    {   
		DListNode tail, temp, p, last;
		tail = head.getPrior();
		head.getPrior().setNext(null);
		head.setPrior(null);
		temp = head;
		head = tail;
		tail = temp;
		p = head;
		
		while(p != null)
		{
			temp = p.getNext();
			p.setNext(p.getPrior());
			p.setPrior(temp);
			p = p.getNext();
		}
		head.setPrior(tail);
		tail.setNext(head);
		
	}

} // class DList