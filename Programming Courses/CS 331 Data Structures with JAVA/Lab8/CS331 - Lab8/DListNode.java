// Facilitator class for the List class
class DListNode                     // A doubly linked list node
{
    // Data members
    private Object element;         // List element
    private DListNode prior,        // Reference to the previous element
                      next;         // Reference to the next element

    // Constructor
    DListNode(Object elem)
    { 
		element = elem;

	}

    // Class Methods used by client class
    DListNode getNext( )                    // Return reference to next element
    {     
		return next;
	}
    DListNode setNext( DListNode nextVal )  
    // Set reference to next element & return that reference
    {    
		next = nextVal;
		return next;
	}
    DListNode getPrior( )                   // Return reference to prior element
    {      
		return prior;
	}
    DListNode setPrior( DListNode priorVal )    
    // Set reference to prior element & return that reference
    {           
		prior = priorVal;
		return prior;
    }
    Object getElement( )                    // Return the element in the current node
    {          
		return element;
	}
    void setElement(Object elem)          
    // Set value of the element in the current node
    {    
		element = elem;
	}

} // class DListNode