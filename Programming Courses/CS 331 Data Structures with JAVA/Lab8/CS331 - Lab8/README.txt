---------------------------------
Bridge
---------------------------------
Test #1

Test Case		Commands	Expected Result	Checked

Insert at end		+a +b +c +d	a b c D		yes
Travel from start	< N N 		a b C d		yes
Travel from end		> P P		a B c d		yes
Delete middle		-		a C d		yes
Insert at middle	+e +f +f	a c e f F d	yes
Remove last		> -		A c e f f	yes
Remove first		-		C e f f		yes
Display element		@		Returns c	yes
Replace element		=g		G e f f		yes
Clear the list		C		Empty List	yes
---------------------------------
In-Lab #1
---------------------------------
Test

Test case	Commands	Expected Result	Checked

Set up List	+a +b +c +d	a b c D		yes
Reverse List	R		D c b a		yes
Remove Last	> -		D c b		yes
Reverse List	R		b c D 		yes
add element	+f		b c d F		yes
Reverse List	R		F d c b		yes														
---------------------------------
Postlab #1
---------------------------------
Insert
O(1)
There is no looping in the method as it only goes through each line once.

Remove
O(1)
There is no looping in the method as it only goes through each line once thanks
to the getPrior method in the DListNode class.

gotoNext()
O(1)
Same reasoning behind the Insert method.

gotoPrev()
O(1)
Same reasoning behind the Remove method.


