import java.io.*;
import java.util.*;

public class Logbook
{
	// Data members
	private int logMonth,						// Logbook month
				logYear;						// Logbook year
	private int[] entry = new int[31];			// Array of Logbook entries
	private GregorianCalendar logCalendar;		// Java's built-in Calendar class
	
	// Constructor
	
	public Logbook ( )
    // Default constructor. Creates a logbook for the current month/year.
    {
		logCalendar = new GregorianCalendar(2015, 4, 1);
		logMonth = 5;
		logYear = 2015;
    }
	public Logbook (int month, int year)
	{
		int j = 0;
		if ((month > 12) || (month < 0))
		{
			logCalendar = new GregorianCalendar(2015, 4, 1);
			logMonth = 5;
			logYear = 2015;
		}
		else
		{
			logCalendar = new GregorianCalendar(year, month - 1, 1);
			logMonth = month;
			logYear = year;
		}
		
		for(int i = 0; i <= daysInMonth() - 1; i++)
		{
			entry[i] = 0;
		}
	}
	
	// Logbook marking operations/methods
	public void putEntry (int day, int value)
	{
		entry[day-1] = value;
	}
	
	public int getEntry (int day)
	{
		return entry[day-1];
	}
	
	// General operations/methods
	public int month()
	{
		return logMonth;
	}
	
	public int year()
	{
		return logYear;
	}
	
	public int daysInMonth()
	{
		if ((logMonth == 2) && (leapYear() == true))
		{
			return 29;
		}
		else if (((logMonth == 2) && (leapYear() == false)))
		{
			return 28;
		}
			
		if ((logMonth == 4) || (logMonth == 6) || (logMonth == 9) || (logMonth == 11))
		{
			return 30;
		}
		else
		{
			return 31;
		}
	}
	
	private boolean leapYear()
	{
		return (logCalendar.isLeapYear(logYear));
	}
	
	private int dayOfWeek ( int day )
    // Returns the day of the week corresponding to the specified day.
    {
		logCalendar.set(logYear, logMonth - 1, day);
		return logCalendar.get(Calendar.DAY_OF_WEEK);
    }
	
	public void displayCalendar ()
    // Displays a logbook using the traditional calendar format.
    {
		System.out.println("\t\t\t"+logMonth+" / "+logYear+"\n");
		System.out.println("Sun\tMon\tTue\tWed\tThu\tFri\tSat\n");
		int dayCount = 1;
		if(dayOfWeek(1) == 1)
		{
			
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 2)
		{
			System.out.print("\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 3)
		{
			System.out.print("\t\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 4)
		{
			System.out.print("\t\t\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 5)
		{
			System.out.print("\t\t\t\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 6)
		{
			System.out.print("\t\t\t\t\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		else if(dayOfWeek(1) == 7)
		{
			System.out.print("\t\t\t\t\t\t");
			for(int i = 1; i <= daysInMonth(); i++)
			{			
				System.out.print(dayCount+" "+getEntry(dayCount)+"\t");
				dayCount++;
				if(dayOfWeek(i) == 7)
				{
					System.out.println("\n");
				}
			}
		}
		
		

    }
	
	
}