---------------------------------
Bridge
---------------------------------
Test1

Test case		Logbook month	No. days in month	Checked

Simple month		 1 2002		31			yes
Month in the past	 7 1998		31			yes
Month in the future	12 2008		31			yes
Current month		 5 2015		31			yes
Feb (not leap year)	 2 1999		28			yes
Feb (leap year)		 2 2000		29			yes
An invalid month	13 2002		31			yes

Test2

Test case			Logbook Entries		Expected result		Checked
1st and 15th of month	 	 1 100, 15 200		 1 100, 15 200		yes
1st and last of month		 1 250, 31 300		 1 250, 31 300		yes
All fridays		 	 1  50,  8 100		 1  50,  8 100		yes
				15 150, 22 200		15 150, 22 200
				29 250				29 250	
Change 1st day			1 100,  1 300		 1 100,  1 300		yes

---------------------------------
In-Lab #1
---------------------------------
Test3

Test case		Logbook month	1st day of the month	Checked

Simple month		 1 2002		6 (Saturday)		yes
Month in the past	 7 1998		3 (Wednesday)		yes
Month in the future	12 2008		1 (Monday)		yes
Current month		 5 2015		5 (Friday)		yes
Feb (not leap year)	 2 2002		5 (Friday)		yes
Feb (leap year)		 2 2000		2 (Tuesday)		yes
																
---------------------------------
Postlab #1
---------------------------------
Part A
If (logYear % 4 == 0)
{
	return true;
}
else
{
	return false;
}

Part B
It costs you having to go into another class to do what you need to get done.
It saves a few lines of code in our class.

Implementing leapYear() in our class saves us from having to go into another class.
It costs us a few more lines of code in our class.

