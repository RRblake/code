*-----------------------------------------------------------
* Title      :
* Written by :
* Date       :
* Description:
*-----------------------------------------------------------
 ORG $1000 *Program starts. 
 MOVE.L #$082000,A5 
 MOVE.L #$0827FF,A6 
LOOP1 MOVE.B #$AA,(A5)+ *Fill in memory with $AA. 
 CMPA.L A5,A6 *Compare A5 and A6. 
 BNE LOOP1 *If not equal, then branch to LOOP1. 
 MOVE.L #$086000,A5 *Copy content of $86000 to A5. 
 CMP.B #$AA,(A5) *Compare A5 content to hex value AA. 
 BNE LOOP3 *If not equal, then branch to LOOP3. 
LOOP2 MOVE.B #$55,(A5)+ *Copy $55 if $AA is found. 
 CMP.L A5,A6 *Compare A5 and A6. 
 BEQ EXIT1 *If equal, then branch to EXIT 
 CMP.B #$AA,(A5) *Compare A5 content to hex value AA. 
 BEQ LOOP2 *If equal, then branch to LOOP2. 
LOOP3 MOVE.L A5,D0 *Not equal, copy to D0 the address of A5. 
 MOVE.B #248,D7 *Output address of wrong character. 
 TRAP #14 
 MOVE.B #$20,D0 *Output a blank space. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B #$AA,D0 *Output AA. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B #$20,D0 *Output a space. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B (A5),D0 *Output wrong character. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B #$20,D0 *Output a blank space. 
 MOVE.B #248,D7 
 TRAP #14 
EXIT1 ORG $2000 *Next part of the program begins. 
 MOVE.L #$082000,A5 
 CMP.B #$55,(A5) *Compare A5 and hex value of 55. 
 BNE LOOP5 *If not equal, then branch to LOOP5. 
LOOP4 MOVE.B #$AA,(A5)+ *Store hex value AA to A5 and increment. 
 CMP.L A5,A6 *Compare registers A5 and A6. 
 BEQ EXIT2 *If equal, then branch to EXIT2. 
 CMP.B #$55,(A5) *Compare A5 and hex value of 55. 
 BEQ LOOP4 *If equal, then branch to LOOP4. 
 
LOOP5 MOVE.L A5,D0 *Copy A5 to D0. 
 MOVE.B #248,D7 *Output the address where failure *occurred.
 TRAP #14 
 MOVE.B #$20,D0 *Output a blank space. 
 MOVE.B #248,D7 
 TRAP #14 MOVE.B #$55,D0 *Output hex value 55. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B #$20,D0 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B (A5),D0 *Output a bad character. 
 MOVE.B #248,D7 
 TRAP #14 
 MOVE.B #$20,D0 
 MOVE.B #248,D7 
 TRAP #14 
EXIT2 MOVE.B #228,D7 *Return to prompt. 
 TRAP #14
*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
