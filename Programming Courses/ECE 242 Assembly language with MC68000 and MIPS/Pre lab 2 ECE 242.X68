*-----------------------------------------------------------
* Title      :
* Written by :
* Date       :
* Description:
*-----------------------------------------------------------
    
                 

numINPUT    DC.B    'Type a 4 digit Hexadecimal Number: '
            DC.B    0  
            DS.W    0
ERROR       DC.B    'ERROR'
            DC.B    0
            DS.W    0
START    
    ORG     $3000
    
            MOVEA.L #numINPUT,A1
            MOVE.L  #14,D0
            TRAP    #15
            
            MOVE.L  #4,D0
            TRAP    #15
            MOVE D1,D6
            
            MOVE.B  #0,D7
            CMP.B   #$30,D6
            BLT.B   ERR
            CMP.B   #$39,D6
            BGT.B   ALPHA
            SUBI.B  #$30,D6
            BRA     end
            
ERR         MOVE.B  #80,D1
            MOVE.B  #1,D0
            MOVEA.L #ERROR,A1
            TRAP    #15
            MOVE.B  #1,D7
            BRA     end
            
ALPHA       CMPI.B  #$41,D6
            BLT.B   ERR
            CMPI.B  #$46,D6
            BGT.B   ERR
            SUBI.B  #$37,D6
end            
            
            
            
            
    
    
    SIMHALT             ; halt simulator

* Put variables and constants here

    END    START        ; last line of source

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
