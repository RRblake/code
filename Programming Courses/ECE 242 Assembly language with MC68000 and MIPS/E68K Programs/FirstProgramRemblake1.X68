*-----------------------------------------------------------
* Title      : First Program
* Written by : Randall Remblake
* Date       : 1/16/2014
* Description: A first program for the ECE 242 course.
*-----------------------------------------------------------
    ORG    $1000
START:                  ; first instruction of program

* Bit is one binary digit. This is in fact the smallest unit of information
* Nibble equals four bits and a byte equals 8 bits.
* I am a real expert with these terms.
* A word refers to the number of bits (data) the CPU handles as one unit.
* For our case, it is 16 bits.
* A longword is double the size of the word.
* Memory is measured in Bytes.
* It is in K's, where 1K = 1,024 Bytes.

    MOVE.B #9,D0
    TRAP   #15


    SIMHALT             ; halt simulator

* Put variables and constants here

    END    START        ; last line of source


*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
