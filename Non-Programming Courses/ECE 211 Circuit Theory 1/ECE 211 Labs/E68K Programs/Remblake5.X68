*-----------------------------------------------------------
* Title      : Assignment 5
* Written by : Randy Remblake
* Date       : 2/18/14
* Description: A program that calculates the net pay for an employee.
*-----------------------------------------------------------
    ORG    $2000
START:

init    
*
*   We are assumings that the employer has provided us with the
*   gross earnings. That value will be D1
*
*   Calculates the federal tax and stores the value into D2.
*                
                MOVE.L  D1,-(SP)                
                MOVEQ   #4,D2
                DIVU    D2,D1
                MOVE.L  D1,D2
                AND.L    #$FFFF,D2
                MOVE.L  (SP)+,D1
*
*   Calculates the Social Security & Medicare tax and stores into D3.
*               
                MOVE.L  D1,-(SP)
                MOVEQ   #10,D3
                DIVU    D3,D1
                MOVE.L  D1,D3
                AND.L    #$FFFF,D3
                MOVE.L  (SP)+,D1
*
*   Calculates the State tax and stores the value into D4.
*                
                MOVE.L  D1,-(SP)
                MOVEQ   #100,D4
                DIVU    D4,D1
                MOVE.L  D1,D4
                AND.L    #$FFFF,D4
                MOVE.L  (SP)+,D1
*
*   Calculates the Net pay by subtacting the gross earnings(D1) by
*   the Federal tax(D2), Social Security & Medicare tax(D3), and the State
*   tax(D4). Then stores the value into D5.
*                
                MOVE.L  D1,D5
                SUB.L   D2,D5
                SUB.L   D3,D5
                SUB.L   D4,D5


    SIMHALT             ; halt simulator

* Put variables and constants here

    END    START        ; last line of source

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
